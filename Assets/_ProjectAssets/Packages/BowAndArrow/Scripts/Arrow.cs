﻿using Defender.Interactions;
using Defender.Kinematics;
using Defender.Networking;
using Photon.Pun;
using Photon.Pun.Simple;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Arrow : MonoBehaviour
{
    private const float MAX_JOINT_MASS_RATIO = 10.0f;

    [field: SerializeField]
    public GrabAttachPoint AutoGrabPoint { get; set; }
    [field: SerializeField]
    private float LaunchSpeed { get; set; } = 2000.0f;
    [field: SerializeField]
    private Transform ColliderObjectTransform { get; set; }
    [field: SerializeField]
    private FlightStabilizer Stabilizer { get; set; }
    [field: SerializeField]
    private GameObject StuckArrowPrefab { get; set; }

    public bool IsLaunched { get; set; } = false;
    public Rigidbody Rigidbody { get; private set; }
    private PhotonView PhotonView { get; set; }
    private ArrowCaster Caster { get; set; }
    private RaycastHit hit;
    private SyncTransform SyncTransform { get; set; }

    protected virtual void Awake()
    {
        Stabilizer.enabled = false;
        Rigidbody = GetComponent<Rigidbody>();
        PhotonView = GetComponent<PhotonView>();
        SyncTransform = GetComponent<SyncTransform>();
        Caster = GetComponent<ArrowCaster>();
    }

    //protected override void OnSelectExited(SelectExitEventArgs args)
    //{
    //    base.OnSelectExited(args);

    //    if (args.interactorObject is Notch notch)
    //    {
    //        if (notch.CanRelease == true)
    //        {
    //            LaunchArrow(notch);
    //        }
    //    }
    //}

    private void LaunchArrow(Notch notch)
    {
        float pullAmount = notch.PullMeasurer.PullAmount;
        ApplyLaunchForce(pullAmount);
        PhotonView.RPC(nameof(ApplyLaunchForce), RpcTarget.Others, pullAmount);
        StartCoroutine(LaunchRoutine());
    }

    private IEnumerator LaunchRoutine()
    {
        WaitForFixedUpdate fixedUpdateWait = new();

        while (Caster.IsColliding(Rigidbody, out hit, out Vector3 localVelocity) == false)
        {
            yield return fixedUpdateWait;
        }

        Vector3 targetTipPosition = hit.point + Rigidbody.velocity * Time.fixedDeltaTime;
        transform.position = Caster.GetArrowPosition(targetTipPosition);

        Vector3 tipVelocity = Rigidbody.GetPointVelocity(targetTipPosition);
        OwnershipCapturer arrowOwnershipAquisitor = GetComponent<OwnershipCapturer>();

        if (IsHittingDynamicObject(hit, out PhotonView hitPhotonView, out Rigidbody hitRigidbody) == true)
        {
            PhysicsData rigidbodiesTree = hitPhotonView.GetComponent<PhysicsData>();
            int rigidbodyIndex = -1;

            if (rigidbodiesTree != null)
            {
                rigidbodyIndex = rigidbodiesTree.Rigidbodies.IndexOf(hitRigidbody);
            }

            transform.SetParent(hitRigidbody.transform);
            Vector3 localPosition = transform.localPosition;
            Quaternion localRotation = transform.localRotation;
            Vector3 localTipVelocity = hitRigidbody.transform.InverseTransformVector(tipVelocity);

            arrowOwnershipAquisitor?.EmulateCollision(hitPhotonView.gameObject);

            // TEST ---------
            float impuleScale = hitRigidbody.gameObject.layer == LayerMask.NameToLayer("Enemies") ? 10.0f : 1.0f;
            hitRigidbody.AddForceAtPosition(tipVelocity * Rigidbody.mass * impuleScale, Caster.GetTipPosition(), ForceMode.Impulse);
            // --------------

            PhotonView.RPC(nameof(ResolveHit), RpcTarget.AllBuffered, localPosition, localRotation, localTipVelocity, hitPhotonView.ViewID, rigidbodyIndex);
        }
        else
        {
            PhotonView.RPC(nameof(ResolveHit), RpcTarget.AllBuffered, transform.position, transform.rotation, tipVelocity, -1, -1);
        }

        Destroy(arrowOwnershipAquisitor);
        PhotonNetwork.Destroy(gameObject);
    }

    [PunRPC]
    private void ApplyLaunchForce(float pullAmount)
    {
        IsLaunched = true;
        Rigidbody.AddForce(transform.forward * (pullAmount * LaunchSpeed));
        Stabilizer.enabled = true;
    }

    [PunRPC]
    private void ResolveHit(Vector3 arrowPosition, Quaternion arrowRotation, Vector3 tipVelocity, int viewID, int rigidbodyIndex)
    {
        GameObject stuckArrow = Instantiate(StuckArrowPrefab);

        if (viewID > 0)
        {
            PhotonView hitPhotonView = PhotonView.Find(viewID);
            Rigidbody hitRigidbody;

            if (rigidbodyIndex < 0)
            {
                hitRigidbody = hitPhotonView.GetComponent<Rigidbody>();
            }
            else
            {
                PhysicsData rigidbodiesTree = hitPhotonView.GetComponent<PhysicsData>();
                hitRigidbody = rigidbodiesTree.Rigidbodies[rigidbodyIndex];
            }

            // Parenting version
            //stuckArrow.transform.SetParent(hitRigidbody.transform);
            //stuckArrow.transform.localPosition = arrowPosition;
            //stuckArrow.transform.localRotation = arrowRotation;

            // Fixed joint version
            stuckArrow.transform.localPosition = hitRigidbody.transform.TransformPoint(arrowPosition);
            stuckArrow.transform.localRotation = hitRigidbody.transform.rotation * arrowRotation;
            FixedJoint fixedJoint = stuckArrow.AddComponent<FixedJoint>();
            fixedJoint.connectedBody = hitRigidbody;

            float massRatio = hitRigidbody.mass / Rigidbody.mass;

            if (massRatio > MAX_JOINT_MASS_RATIO)
            {
                fixedJoint.connectedMassScale = (MAX_JOINT_MASS_RATIO * Rigidbody.mass) / hitRigidbody.mass;
            }

            stuckArrow.GetComponent<StuckArrow>().SetMeshParent(hitRigidbody.transform);

            // Currently invoked only for dynamic objects
            InvokeOnHitMethods(hitRigidbody.gameObject);
        }
        else
        {
            stuckArrow.transform.position = arrowPosition;
            stuckArrow.transform.rotation = arrowRotation;
            stuckArrow.GetComponent<StuckArrow>().SetMeshParent(null);
        }


    }

    private void InvokeOnHitMethods(GameObject gameObject)
    {
        IArrowHittable arrowHitComponent = gameObject.GetComponent<IArrowHittable>();
        arrowHitComponent?.Hit(this);
    }

    private bool IsHittingDynamicObject(RaycastHit hit, out PhotonView hitPhotonView, out Rigidbody hitRigidbody)
    {
        hitPhotonView = hit.collider.GetComponentInParent<PhotonView>();

        if (hitPhotonView != null)
        {
            hitRigidbody = hit.collider.GetComponentInParent<Rigidbody>();
            return hitRigidbody != null;
        }

        hitRigidbody = null;
        return false;
    }
}
