﻿using Defender.Interactions;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class Quiver : InteractableBase
    {
        [field: SerializeField]
        private GameObject ProjectilePrefab { get; set; }

        public override bool IsInteracting => false;

        protected override void OnInteractionStarted(InteractionInfo interaction)
        {
            GameObject projectileObject = InstantiateProjectile(interaction.Interactor.transform);
            Arrow projectile = projectileObject.GetComponent<Arrow>();
            GrabArrow(projectile, interaction.Interactor);
        }

        private GameObject InstantiateProjectile(Transform interactorTransform)
        {
            Transform autoGrabPointTransform = ProjectilePrefab.GetComponent<Arrow>().AutoGrabPoint.transform;
            Vector3 rotatedGrabPointPosition = Quaternion.Inverse(autoGrabPointTransform.localRotation) * autoGrabPointTransform.localPosition;
            Vector3 spawnPosition = interactorTransform.TransformPoint(rotatedGrabPointPosition * -1.0f);
            Quaternion spawnRotation = interactorTransform.rotation * Quaternion.Inverse(autoGrabPointTransform.localRotation);
            return PhotonNetwork.Instantiate(ProjectilePrefab.name, spawnPosition, spawnRotation);
        }

        private void GrabArrow(Arrow projectile, Interactor interactor)
        {
            GrabInteractable grabInteractable = projectile.GetComponent<GrabInteractable>();
            Vector3 grabPoint = projectile.AutoGrabPoint.transform.position;
            Collider cloasestCollider = grabInteractable.PhysicsData.GetCloasestCollider(grabPoint, out Vector3 cloasestPointOnCollider);
            interactor.StartInteraction(grabInteractable, cloasestCollider, cloasestPointOnCollider);
        }
    }
}
