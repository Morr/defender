using Photon.Pun;
using UnityEngine;

public class ArrowCaster : MonoBehaviourPun
{
    [field: SerializeField]
    private Transform TipTransform { get; set; }
    [field: SerializeField]
    private LayerMask LayerMask { get; set; } = ~0;

    public bool IsColliding(Rigidbody rigidbody, out RaycastHit hit, out Vector3 localVelocity)
    {
        localVelocity = rigidbody.transform.InverseTransformVector(rigidbody.velocity);

        if (localVelocity.z > 0.0f)
        {
            return Physics.Linecast(TipTransform.position, TipTransform.position + rigidbody.velocity * Time.fixedDeltaTime, out hit, LayerMask, QueryTriggerInteraction.Ignore);
        }

        hit = default;
        return false;
    }

    public Vector3 GetArrowPosition(Vector3 tipPosition)
    {
        Vector3 offset = transform.position - TipTransform.position;
        return tipPosition + offset;
    }

    public Vector3 GetTipPosition()
    {
        return TipTransform.position;
    }
}
