using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class StuckArrow : MonoBehaviour
    {
        [field: SerializeField]
        private GameObject MeshObject { get; set; }

        public void SetMeshParent(Transform meshParent)
        {
            MeshObject.transform.SetParent(meshParent);
        }

        protected virtual void OnDestroy()
        {
            Destroy(MeshObject);
        }
    }
}

