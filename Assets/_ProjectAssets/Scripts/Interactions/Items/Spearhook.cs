using Defender.Networking;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Interactions
{
    [RequireComponent(typeof(PhotonView), typeof(XRGrabNetworkInteractable))]
    public class Spearhook : MonoBehaviour
    {
        [field: SerializeField]
        private ConfigurableJoint ClipJoint { get; set; }
        [field: SerializeField]
        private float LockAngle { get; set; }
        [field: SerializeField]
        private float UnlockAngle { get; set; }

        private PhotonView PhotonView { get; set; }
        private XRGrabNetworkInteractable GrabComponent { get; set; }

        protected virtual void Awake()
        {
            PhotonView = GetComponent<PhotonView>();
            GrabComponent = GetComponent<XRGrabNetworkInteractable>();
            GrabComponent.activated.AddListener(OnActivated);
            GrabComponent.deactivated.AddListener(OnDeactivated);
            GrabComponent.selectExited.AddListener(OnDeselected);
        }

        protected virtual void OnDestroy()
        {
            GrabComponent.activated.RemoveListener(OnActivated);
            GrabComponent.deactivated.RemoveListener(OnDeactivated);
            GrabComponent.selectExited.RemoveListener(OnDeselected);
        }

        private void OnDeactivated(DeactivateEventArgs activateArgs)
        {
            SetLockStateLocally(true);
        }

        private void OnDeselected(SelectExitEventArgs deselectArgs)
        {
            SetLockStateLocally(true);
        }

        private void OnActivated(ActivateEventArgs deactivateArgs)
        {
            SetLockStateLocally(false);
        }

        private void SetLockStateLocally(bool isLocked)
        {
            PhotonNetwork.RemoveBufferedRPCs(PhotonView.ViewID, nameof(SetLockState));
            PhotonView.RPC(nameof(SetLockState), RpcTarget.AllBuffered, isLocked);
        }


        [PunRPC]
        private void SetLockState(bool isLocked)
        {
            ClipJoint.targetRotation = isLocked == true ? Quaternion.Euler(LockAngle, 0.0f, 0.0f) : Quaternion.Euler(UnlockAngle, 0.0f, 0.0f);
        }
    }
}
