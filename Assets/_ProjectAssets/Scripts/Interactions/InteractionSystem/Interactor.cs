using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Interactions
{
    public class Interactor : MonoBehaviour
    {
        private const int MAX_OVERLAP_COLLIDERS = 32;

        [field: SerializeField]
        public XRBaseController Controller { get; private set; }
        [field: SerializeField]
        private LayerMask LayerMask { get; set; } = ~0;
        [field: SerializeField]
        private InputActionReference SelectAction { get; set; }
        [field: SerializeField]
        private float ScanRange { get; set; } = 0.1f;

        public Action<InteractionInfo> InteractionStarted { get; set; } = delegate { };
        public Action<InteractionInfo> InteractionEnded { get; set; } = delegate { };

        private InteractionInfo CurrentInteraction { get; set; }
        private Collider[] ScanColliders { get; set; }

        public void StartInteraction(InteractableBase interactable, Collider collider, Vector3 pointOnCollider)
        {
            EndCurrentInteraction();
            CurrentInteraction = new InteractionInfo(InteractionInfo.InteractionType.Grab, this, interactable, collider, pointOnCollider);

            if (interactable.TryStartInteraction(CurrentInteraction) == true)
            {
                interactable.InteractionInterrupted += OnInteractionInterrupted;
                InteractionStarted.Invoke(CurrentInteraction);
            }
            else
            {
                CurrentInteraction = null;
            }
        }

        protected virtual void Awake()
        {
            ScanColliders = new Collider[MAX_OVERLAP_COLLIDERS];
        }

        protected virtual void OnEnable()
        {
            SelectAction.action.performed += OnSelectActionPerformed;
            SelectAction.action.canceled += OnSelectActionCanceled;
        }

        protected virtual void OnDisable()
        {
            SelectAction.action.performed -= OnSelectActionPerformed;
            SelectAction.action.canceled -= OnSelectActionCanceled;
        }

        private void OnSelectActionPerformed(InputAction.CallbackContext context)
        {
            ScanAndStartInteraction();
        }

        private void OnSelectActionCanceled(InputAction.CallbackContext context)
        {
            EndCurrentInteraction();
        }

        private void OnInteractionInterrupted(InteractionInfo interaction)
        {
            if (interaction == CurrentInteraction)
            {
                EndCurrentInteraction();
            }
        }

        private void ScanAndStartInteraction()
        {
            if (ScanForInteractable(out InteractableBase interactable, out Collider collider, out Vector3 pointOnCollider) == true)
            {
                StartInteraction(interactable, collider, pointOnCollider);
            }
        }

        private void EndCurrentInteraction()
        {
            if (CurrentInteraction != null)
            {
                InteractableBase interactable = CurrentInteraction.Interactable;
                interactable.InteractionInterrupted -= OnInteractionInterrupted;
                interactable.EndInteraction(CurrentInteraction);
                InteractionEnded.Invoke(CurrentInteraction);
                CurrentInteraction = null;
            }
        }

        private bool ScanForInteractable(out InteractableBase interactable, out Collider collider, out Vector3 pointOnCollider)
        {
            interactable = null;
            collider = null;
            pointOnCollider = default;

            int scanCollidersCount = Physics.OverlapSphereNonAlloc(transform.position, ScanRange, ScanColliders, LayerMask, QueryTriggerInteraction.Collide);
            float minDistance = Mathf.Infinity;

            for (int i = 0; i < scanCollidersCount; i++)
            {
                Collider scanCollider = ScanColliders[i];
                Vector3 pointOnScanCollider = scanCollider.ClosestPoint(transform.position);
                float distance = Vector3.Distance(transform.position, pointOnScanCollider);

                if (distance < minDistance)
                {
                    InteractableBase scanInteractable = InteractablesManager.Instance.GetInteractable(scanCollider);

                    if (scanInteractable != null)
                    {
                        interactable = scanInteractable;
                        collider = scanCollider;
                        pointOnCollider = pointOnScanCollider;
                        minDistance = distance;
                    }
                }
            }

            return interactable != null;
        }
    }
}
