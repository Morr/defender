using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    //[RequireComponent(typeof(Interactor))]
    public abstract class InteractorExtensionBase : MonoBehaviour
    {
        protected Interactor Interactor { get; set; }

        private void Start()
        {
            Interactor = GetComponent<Interactor>();
            Interactor.InteractionStarted += OnInteractionStarted;
            Interactor.InteractionEnded += OnInteractionEnded;
        }

        private void OnDestroy()
        {
            if (gameObject.scene.isLoaded == true)
            {
                Interactor.InteractionStarted -= OnInteractionStarted;
                Interactor.InteractionEnded -= OnInteractionEnded;
            }
        }

        protected abstract void OnInteractionStarted(InteractionInfo interaction);
        protected abstract void OnInteractionEnded(InteractionInfo interaction);
    }
}
