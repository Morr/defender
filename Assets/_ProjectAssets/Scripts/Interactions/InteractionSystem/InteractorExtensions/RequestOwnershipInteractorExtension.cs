using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Interactions
{
    public class RequestOwnershipInteractorExtension : InteractorExtensionBase
    {
        protected override void OnInteractionStarted(InteractionInfo interactionInfo)
        {
            CreateOrRefreshOwnerhsipAquisitor(interactionInfo.Interactable);
        }

        protected override void OnInteractionEnded(InteractionInfo interaction)
        {
        }

        private void CreateOrRefreshOwnerhsipAquisitor(InteractableBase interactable)
        {
            List<Rigidbody> rigidbodies = interactable.PhysicsData.Rigidbodies;
            
            for (int i = 0; i < rigidbodies.Count; i++)
            {
                Rigidbody rigidbody = rigidbodies[i];
                OwnershipCapturer capturer = rigidbody.gameObject.GetOrAddComponent<OwnershipCapturer>();
                capturer.enabled = true;
            }
        }
    }
}
