using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class GrabAttachPoint : MonoBehaviour
    {
        [field: SerializeField]
        public float SnapRange { get; private set; }

        public void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1.0f, 1.0f, 0.0f, 0.4f);
            Gizmos.DrawSphere(transform.position, SnapRange);
        }
    }
}

