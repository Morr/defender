using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [RequireComponent(typeof(PhysicsData))]
    public abstract class InteractableBase : MonoBehaviour
    {
        public Action<InteractionInfo> InteractionStarted { get; set; } = delegate { };
        public Action<InteractionInfo> InteractionEnded { get; set; } = delegate { };
        public Action<InteractionInfo> InteractionInterrupted { get; set; } = delegate { };

        public abstract bool IsInteracting { get; }
        public PhysicsData PhysicsData { get; private set; }

        public bool TryStartInteraction(InteractionInfo interaction)
        {
            if (CheckInteractionCondition(interaction) == true)
            {
                OnInteractionStarted(interaction);
                InteractionStarted.Invoke(interaction);
                return true;
            }

            return false;
        }

        public void EndInteraction(InteractionInfo interaction)
        {
            OnInteractionEnded(interaction);
            InteractionEnded.Invoke(interaction);
        }

        public virtual void Interrupt(InteractionInfo interaction)
        {
            InteractionInterrupted.Invoke(interaction);
        }

        protected virtual void Awake()
        {
            PhysicsData = GetComponent<PhysicsData>();
            InteractablesManager.Instance.RegisterInteractable(this);
        }

        protected virtual void OnDestroy()
        {
            if (gameObject.scene.isLoaded == true)
            {
                InteractablesManager.Instance.UnregisterInteractable(this);
            }
        }


        protected virtual bool CheckInteractionCondition(InteractionInfo interaction)
        {
            return true;
        }

        protected virtual void OnInteractionStarted(InteractionInfo interaction) { }

        protected virtual void OnInteractionEnded(InteractionInfo interaction) { }
    }
}

