using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [CreateAssetMenu(menuName = nameof(Defender) + "/Interactables/GrabProfile")]
    public class GrabProfile : ScriptableObject
    {
        [field: SerializeField]
        public float VelocityLimit { get; private set; }
        [field: SerializeField]
        public float AngularVelocityLimit { get; private set; }

        [field: SerializeField, Space]
        private ConfigurableJointMotion MotionConstraints { get; set; }
        [field: SerializeField]
        private ConfigurableJointMotion AngularMotionConstraints { get; set; }
        [field: SerializeField]
        private float BreakForce { get; set; }
        [field: SerializeField]
        private SoftJointLimitSerializationHelper LinearLimit { get; set; }
        [field: SerializeField]
        private SoftJointLimitSpringSerializationHelper LinearSpring { get; set; }
        [field: SerializeField]
        private JointDriveSerializationHelper AngularDrive { get; set; }

        public void ApplyTo(ConfigurableJoint joint)
        {
            joint.xMotion = MotionConstraints;
            joint.yMotion = MotionConstraints;
            joint.zMotion = MotionConstraints;

            joint.angularXMotion = AngularMotionConstraints;
            joint.angularYMotion = AngularMotionConstraints;
            joint.angularZMotion = AngularMotionConstraints;

            joint.breakForce = BreakForce;
            joint.linearLimit = LinearLimit.GetValue();
            joint.linearLimitSpring = LinearSpring.GetValue();

            joint.angularXDrive = AngularDrive.GetValue();
            joint.angularYZDrive = AngularDrive.GetValue();
        }

        [System.Serializable]
        private struct SoftJointLimitSerializationHelper
        {
            [field: SerializeField]
            public float Bounciness { get; set; }
            [field: SerializeField]
            public float ContactDistance { get; set; }
            [field: SerializeField]
            public float Limit { get; set; }

            public SoftJointLimit GetValue()
            {
                return new SoftJointLimit()
                {
                    bounciness = Bounciness,
                    contactDistance = ContactDistance,
                    limit = Limit
                };
            }
        }

        [System.Serializable]
        private struct SoftJointLimitSpringSerializationHelper
        {
            [field: SerializeField]
            public float Damper { get; set; }
            [field: SerializeField]
            public float Spring { get; set; }

            public SoftJointLimitSpring GetValue()
            {
                return new SoftJointLimitSpring()
                {
                    damper = Damper,
                    spring = Spring
                };
            }
        }

        [System.Serializable]
        private struct JointDriveSerializationHelper
        {
            [field: SerializeField]
            public float PositionSpring { get; set; }
            [field: SerializeField]
            public float PositionDamper { get; set; }
            [field: SerializeField]
            public float MaximumForce { get; set; }

            public JointDrive GetValue()
            {
                return new JointDrive()
                {
                    positionSpring = PositionSpring,
                    positionDamper = PositionDamper,
                    maximumForce = MaximumForce
                };
            }
        }
    }
}