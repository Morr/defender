using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Defender.Rigs;
using Defender.Kinematics;
using Photon.Pun;
using Utilities;

namespace Defender.Interactions
{
    [RequireComponent(typeof(PhotonView))]
    public class GrabInteractable : InteractableBase
    {
        [field: SerializeField]
        public GrabProfile Profile { get; private set; }

        public override bool IsInteracting => Interactions.Count > 0;
        protected Dictionary<InteractionInfo, GrabActuator> Interactions { get; set; } = new();
        private PhotonView InteractablePhotonView { get; set; }
        private GrabAttachPoint[] AttachPoints { get; set; }

        public void InterruptAllInteractions()
        {
            List<InteractionInfo> interactionsToInterrupt = new (Interactions.Keys);

            for (int i = 0; i < interactionsToInterrupt.Count; i++)
            {
                Interrupt(interactionsToInterrupt[i]);
            }
        }

        protected override void OnInteractionStarted(InteractionInfo interaction)
        {
            Rigidbody interactableRigidbody = interaction.Collider.attachedRigidbody;
            GrabActuator actuator = GrabActuator.GetActuator(interaction.Interactor);
            int rigidbodyIndex = PhysicsData.Rigidbodies.IndexOf(interactableRigidbody);
            Vector3 grabPoint = interaction.PointOnCollider;
            Quaternion grabRotation = actuator.transform.rotation; 

            if (TryGetAttachPoint(interaction.PointOnCollider, out Transform attachTransform) == true)
            {
                grabPoint = attachTransform.position;
                grabRotation = attachTransform.rotation;
            }

            Vector3 relativePosition = interactableRigidbody.transform.InverseTransformPoint(grabPoint);

            actuator.JointBreak += OnActuatorJointBreak;
            actuator.GrabObject(InteractablePhotonView.ViewID, rigidbodyIndex, relativePosition, grabRotation);

            InteractablePhotonView.RequestOwnership();
            Interactions.Add(interaction, actuator);
        }

        protected override void OnInteractionEnded(InteractionInfo interaction)
        {
            GrabActuator actuator = GrabActuator.GetActuator(interaction.Interactor);
            actuator.GrabObject(-1, default, default, default);
            actuator.JointBreak -= OnActuatorJointBreak;
            Interactions.Remove(interaction);

            if (IsInteracting == false && InteractablePhotonView.IsMine == true)
            {
                InteractablePhotonView.RPC(nameof(OnOwnershipReleased), RpcTarget.Others);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            InteractablePhotonView = GetComponent<PhotonView>();
            AttachPoints = GetComponentsInChildren<GrabAttachPoint>();
        }

        private void OnActuatorJointBreak(GrabActuator actuator)
        {
            InteractionInfo interaction = null;

            foreach (KeyValuePair<InteractionInfo, GrabActuator> pair in Interactions)
            {
                if (pair.Value == actuator)
                {
                    interaction = pair.Key;
                }
            }

            if (interaction != null)
            {
                Interrupt(interaction);
            }
        }

        private bool TryGetAttachPoint(Vector3 interactionStartPoint, out Transform attachTransform)
        {
            for (int i = 0; i < AttachPoints.Length; i++)
            {
                GrabAttachPoint attachPoint = AttachPoints[i];
                float distance = Vector3.Distance(interactionStartPoint, attachPoint.transform.position);

                if (distance <= attachPoint.SnapRange)
                {
                    attachTransform = attachPoint.transform;
                    return true;
                }
            }

            attachTransform = null;
            return false;
        }

        [PunRPC]
        private void OnOwnershipReleased()
        {
            if (IsInteracting == true)
            {
                InteractablePhotonView.RequestOwnership();
            }
        }
    }
}

