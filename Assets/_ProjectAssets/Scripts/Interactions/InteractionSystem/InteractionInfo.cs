﻿using UnityEngine;

namespace Defender.Interactions
{
    public class InteractionInfo
    {
        public InteractionType Type { get; set; }
        public Interactor Interactor { get; set; }
        public InteractableBase Interactable { get; set; }
        public Collider Collider { get; set; }
        public Vector3 PointOnCollider { get; set; }

        public InteractionInfo(InteractionType type, Interactor interactor, InteractableBase interactable, Collider collider, Vector3 pointOnCollider)
        {
            Type = type;
            Interactor = interactor;
            Interactable = interactable;
            Collider = collider;
            PointOnCollider = pointOnCollider;
        }

        public enum InteractionType
        {
            Grab,
            Activate
        }
    }
}
