using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Interactions
{
    [DefaultExecutionOrder(-10)]
    public class InteractablesManager : MonoBehaviourSingleton<InteractablesManager>
    {
        private Dictionary<Collider, InteractableBase> InteractablesRegistery { get; set; } = new();

        public void RegisterInteractable(InteractableBase interactable)
        {
            foreach (Collider collider in interactable.PhysicsData.Colliders)
            {
                InteractablesRegistery.Add(collider, interactable);
            }
        }

        public void UnregisterInteractable(InteractableBase interactable)
        {
            foreach (Collider collider in interactable.PhysicsData.Colliders)
            {
                InteractablesRegistery.Remove(collider);
            }
        }

        public InteractableBase GetInteractable(Collider collider)
        {
            if (InteractablesRegistery.TryGetValue(collider, out InteractableBase interactable) == true)
            {
                return interactable;
            }

            return null;
        }
    }
}
