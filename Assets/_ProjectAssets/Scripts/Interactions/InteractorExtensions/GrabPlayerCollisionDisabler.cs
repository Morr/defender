using Defender.Rigs;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [RequireComponent(typeof(GrabInteractable))]
    public class GrabPlayerCollisionDisabler : MonoBehaviour
    {
        private GrabInteractable Interactable { get; set; }

        private bool IsPlayerCollisionDisabled { get; set; } = false;
        private CharacterController PlayerCharacterController => LocalPlayer.Instance.CharacterController;

        protected virtual void Awake()
        {
            Interactable = GetComponent<GrabInteractable>();
        }

        protected virtual void OnEnable()
        {
            Interactable.InteractionStarted += OnInteractionStarted;
            Interactable.InteractionEnded += OnInteractionEnded;
        }

        protected virtual void OnDisable()
        {
            Interactable.InteractionStarted -= OnInteractionStarted;
            Interactable.InteractionEnded -= OnInteractionEnded;
        }

        private void OnInteractionStarted(InteractionInfo interaction)
        {
            if (IsPlayerCollisionDisabled == false)
            {
                SetPlayerCollisionsEnabled(false);
                IsPlayerCollisionDisabled = true;
            }
        }

        private void OnInteractionEnded(InteractionInfo interaction)
        {
            if (Interactable.IsInteracting == false)
            {
                SetPlayerCollisionsEnabled(true);
                IsPlayerCollisionDisabled = false;
            }
        }

        private void SetPlayerCollisionsEnabled(bool isEnabled)
        {
            List<Collider> colliders = Interactable.PhysicsData.Colliders;

            for (int i = 0; i < colliders.Count; i++)
            {
                Physics.IgnoreCollision(PlayerCharacterController, colliders[i], !isEnabled);
            }
        }
    }
}
