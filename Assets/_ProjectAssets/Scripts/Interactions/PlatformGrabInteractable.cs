using Defender.Locomotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class PlatformGrabInteractable : GrabInteractable
    {
        [field: SerializeField]
        private PlayerPlatformDetector PlatformDetector { get; set; }

        protected virtual void OnEnable()
        {
            PlatformDetector.PlatformChanged += OnPlatformChanged;
        }

        protected virtual void OnDisable()
        {
            PlatformDetector.PlatformChanged -= OnPlatformChanged;
        }

        protected override bool CheckInteractionCondition(InteractionInfo interaction)
        {
            Transform platformTransform = PlatformDetector.CurrentPlatformTransform;

            if (platformTransform != null)
            {
                InteractableBase platformInteractable = platformTransform.GetComponentInParent<InteractableBase>();
                return platformInteractable != this;
            }

            return true;
        }

        private void OnPlatformChanged(Transform platformTransform)
        {
            if (platformTransform == null)
            {
                return;
            }

            InteractableBase platformInteractable = platformTransform.GetComponentInParent<InteractableBase>();
            
            if (platformInteractable == this)
            {
                InterruptAllGrabInteractions();
            }
        }

        private void InterruptAllGrabInteractions()
        {
            List<InteractionInfo> grabInteractions = new();

            foreach (InteractionInfo interaction in Interactions.Keys)
            {
                if (interaction.Type == InteractionInfo.InteractionType.Grab)
                {
                    grabInteractions.Add(interaction);
                }
            }

            for (int i = 0; i < grabInteractions.Count; i++)
            {
                Interrupt(grabInteractions[i]);
            }
        }
    }
}

