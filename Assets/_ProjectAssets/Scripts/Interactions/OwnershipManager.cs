using System.Collections;
using System.Collections.Generic;
using Defender.Networking;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Defender.Interactions
{
    public class OwnershipManager : MonoBehaviour
    {
        protected virtual void OnEnable()
        {
            NetworkEventSystem.Instance.OwnershipRequested += OnOwnershipRequest;
        }

        protected virtual void OnDisable()
        {
            NetworkEventSystem.Instance.OwnershipRequested -= OnOwnershipRequest;
        }

        private void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
        {
            if (targetView.IsMine == true)
            {
                InteractableBase interactable = targetView.GetComponent<InteractableBase>();

                if (interactable.IsInteracting == false)
                {
                    targetView.TransferOwnership(requestingPlayer);
                }
            }
        }
    }
}
