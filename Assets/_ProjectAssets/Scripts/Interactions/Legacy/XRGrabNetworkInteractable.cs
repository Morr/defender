using Defender.Interactions;
using Photon.Pun;
using Photon.Pun.Simple;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Networking
{
    public class XRGrabNetworkInteractable : XRGrabInteractable
    {
        public PhotonView PhotonView { get; set; }
        private Rigidbody[] InteractableRigidbodies { get; set; }
        private bool[] DefaultUseGravityValues { get; set; }
        private bool IsSelectedByAnyPlayer { get; set; }

        protected override void Awake()
        {
            PhotonView = GetComponent<PhotonView>();
            InteractableRigidbodies = GetComponentsInChildren<Rigidbody>();
            DefaultUseGravityValues = new bool[InteractableRigidbodies.Length];

            for (int i = 0; i < InteractableRigidbodies.Length; i++)
            {
                DefaultUseGravityValues[i] = InteractableRigidbodies[i].useGravity;
            }

            base.Awake();
        }

        protected override void OnSelectEntering(SelectEnterEventArgs eventArgs)
        {
            if (TryGetOwnership() == true)
            {
                PhotonView.RPC("SetRigidbodyProperties", RpcTarget.All, true);
                base.OnSelectEntering(eventArgs);
            }
        }

        protected override void OnSelectExited(SelectExitEventArgs eventArgs)
        {
            base.OnSelectExited(eventArgs);
            PhotonView.RPC("SetRigidbodyProperties", RpcTarget.All, false);
        }

        public bool TryGetOwnership()
        {
            if (IsSelectedByAnyPlayer == false)
            {
                PhotonView.RequestOwnership();
                return true;
            }

            return false;
        }

        [PunRPC]
        protected virtual void SetRigidbodyProperties(bool isSelected)
        {
            IsSelectedByAnyPlayer = isSelected;

            if (PhotonView.IsMine == false)
            {
                OwnershipCapturer ownershipAquisitor = gameObject.GetComponent<OwnershipCapturer>();
                DestroyImmediate(ownershipAquisitor);
            }

            if (base.isSelected == true)
            {
                return;
            }

            if (isSelected == true)
            {
                for (int i = 0; i < InteractableRigidbodies.Length; i++)
                {
                    InteractableRigidbodies[i].useGravity = false;
                }
            }
            else
            {
                for (int i = 0; i < InteractableRigidbodies.Length; i++)
                {
                    InteractableRigidbodies[i].useGravity = DefaultUseGravityValues[i];
                }
            }
        }
    }
}

