using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class ArrowTarget : MonoBehaviour, IArrowHittable
    {
        [field: SerializeField]
        private Material AfterHitMaterial { get; set; }
        [field: SerializeField]
        private Renderer Renderer { get; set; }

        public void Hit(Arrow arrow)
        {
            Renderer.sharedMaterial = AfterHitMaterial;
        }
    }
}
