using Defender.Interactions;
using Defender.Kinematics;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public interface IAttachItemTarget
    {
        public Rigidbody Rigidbody { get; }
        public PhotonView PhotonView { get; }

        public void OnItemAttached(AttachableItem item);

        public void OnItemDetached(AttachableItem item);
    }
}
