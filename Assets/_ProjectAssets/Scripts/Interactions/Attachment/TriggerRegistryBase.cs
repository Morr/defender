using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public abstract class TriggerRegistryBase<T> : MonoBehaviour where T : Component
    {
        private Dictionary<T, int> Ocurrences { get; set; } = new();

        protected abstract void OnTriggerObjectEnter(T component);

        protected abstract void OnTriggerObjectExit(T component);

        private void OnTriggerEnter(Collider other)
        {
            T component = RetriveComponent(other);

            if (component != null)
            {
                if (Ocurrences.ContainsKey(component) == false)
                {
                    Ocurrences.Add(component, 1);
                    OnTriggerObjectEnter(component);
                }
                else
                {
                    Ocurrences[component] += 1;
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            T component = RetriveComponent(other);

            if (component != null)
            {
                Ocurrences[component] -= 1;

                if (Ocurrences[component] == 0)
                {
                    Ocurrences.Remove(component);
                    OnTriggerObjectExit(component);
                }
            }
        }

        private T RetriveComponent(Collider collider)
        {
            return collider.gameObject.GetComponentInParent<T>();
        }
    }
}
