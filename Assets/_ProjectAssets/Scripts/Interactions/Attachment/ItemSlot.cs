using Photon.Pun;
using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    public class ItemSlot : TriggerRegistryBase<AttachableItem>, IAttachItemTarget
    {
        [field: SerializeField]
        private SerializableDictionaryBase<string, Transform> Entries { get; set; }
        [field: SerializeField]
        private List<Collider> IgnoredColliders { get; set; }

        public Rigidbody Rigidbody { get; private set; }
        public PhotonView PhotonView { get; private set; }
        private AttachableItem AttachedItem { get; set; }

        public void OnItemAttached(AttachableItem item)
        {
            AttachedItem = item;
            SetCollisionsIgnored(true);
            item.Interactable.InteractionStarted += OnAttachedItemInteractionStarted;
        }

        public void OnItemDetached(AttachableItem item)
        {
            SetCollisionsIgnored(false);
            AttachedItem = null;
            item.Interactable.InteractionStarted -= OnAttachedItemInteractionStarted;
        }

        public void RegisterIgnoredCollider(Collider collider)
        {
            IgnoredColliders.Add(collider);
        }

        public void UnregisterIgnoredCollider(Collider collider)
        {
            IgnoredColliders.Remove(collider);
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            PhotonView = GetComponent<PhotonView>();
        }

        protected override void OnTriggerObjectEnter(AttachableItem attachableItem)
        {
            attachableItem.Interactable.InteractionEnded += OnTrackedObjectInteractionEnded;
        }

        protected override void OnTriggerObjectExit(AttachableItem attachableItem)
        {
            attachableItem.Interactable.InteractionEnded -= OnTrackedObjectInteractionEnded;
        }

        private void SetCollisionsIgnored(bool isIgnored)
        {
            List<Collider> attachableColliders = AttachedItem.Interactable.PhysicsData.Colliders;

            for (int i = 0; i < IgnoredColliders.Count; i++)
            {
                for (int j = 0; j < attachableColliders.Count; j++)
                {
                    Physics.IgnoreCollision(IgnoredColliders[i], attachableColliders[j], isIgnored);
                }
            }
        }

        private void OnTrackedObjectInteractionEnded(InteractionInfo interaction)
        {
            AttachableItem attachableItem = interaction.Interactable.GetComponent<AttachableItem>();

            if (attachableItem != null)
            {
                TryAttachItem(attachableItem);
            }
        }

        private bool TryAttachItem(AttachableItem attachableItem)
        {
            if (AttachedItem == null)
            {
                if (Entries.TryGetValue(attachableItem.ItemIdentifier, out Transform attachTransform) == true)
                {
                    Vector3 relativePosition = transform.InverseTransformPoint(attachTransform.position);
                    Quaternion relativeRotation = Quaternion.Inverse(transform.rotation) * attachTransform.rotation;
                    attachableItem.Attach(this, relativePosition, relativeRotation);
                    return true;
                }
            }

            return false;
        }

        private void OnAttachedItemInteractionStarted(InteractionInfo interaction)
        {
            interaction.Interactable.GetComponent<AttachableItem>()?.Detach();
        }
    }
}
