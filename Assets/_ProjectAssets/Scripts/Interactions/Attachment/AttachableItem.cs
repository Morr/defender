using Defender.Kinematics;
using Photon.Pun;
using Photon.Pun.Simple;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [RequireComponent(typeof(Rigidbody), typeof(PhotonView))]
    public class AttachableItem : MonoBehaviour
    {
        [field: SerializeField]
        public Rigidbody Rigidbody { get; private set; }
        [field: SerializeField]
        public string ItemIdentifier { get; set; }

        public GrabInteractable Interactable { get; private set; }
        public PhotonView PhotonView { get; private set; }
        private SyncTransform SyncTransform { get; set; }

        private FixedJoint Joint { get; set; }
        private IAttachItemTarget CurrentAttachTarget { get; set; }

        public virtual void Attach(IAttachItemTarget target, Vector3 relativePosition, Quaternion relativeRotation)
        {
            if (CurrentAttachTarget == null)
            {
                PhotonView.RPC(nameof(AttachRPC), RpcTarget.AllBuffered, target.PhotonView.ViewID, relativePosition, relativeRotation);
            }
        }

        public virtual void Detach()
        {
            PhotonView.RPC(nameof(DetachRPC), RpcTarget.AllBuffered);
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            PhotonView = GetComponent<PhotonView>();
            SyncTransform = GetComponent<SyncTransform>();
            Interactable = GetComponent<GrabInteractable>();
        }

        [PunRPC]
        private void AttachRPC(int viewId, Vector3 relativePosition, Quaternion relativeRotation)
        {
            CurrentAttachTarget = PhotonNetwork.GetPhotonView(viewId).GetComponent<IAttachItemTarget>();

            transform.position = CurrentAttachTarget.Rigidbody.transform.TransformPoint(relativePosition);
            transform.rotation = CurrentAttachTarget.Rigidbody.transform.rotation * relativeRotation;

            Joint = gameObject.AddComponent<FixedJoint>();
            Joint.connectedBody = CurrentAttachTarget.Rigidbody;

            SyncTransform.enabled = false;
            PhysicsControlManager.Instance.NotifyAttachStatusChanged(PhotonView);
            CurrentAttachTarget.OnItemAttached(this);
        }

        [PunRPC]
        private void DetachRPC()
        {
            DestroyImmediate(Joint);
            SyncTransform.ClearCachedFrames();
            SyncTransform.enabled = true;
            PhysicsControlManager.Instance.NotifyAttachStatusChanged(PhotonView);
            CurrentAttachTarget.OnItemDetached(this);
            CurrentAttachTarget = null;
        }
    }
}
