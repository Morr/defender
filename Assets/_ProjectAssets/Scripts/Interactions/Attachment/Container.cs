using Defender.Kinematics;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Interactions
{
    [RequireComponent(typeof(PhotonView), typeof(Rigidbody))]
    public class Container : MonoBehaviour, IAttachItemTarget
    {
        [field: SerializeField]
        public bool IsOpened { get; private set; }
        [field: SerializeField]
        private BoxCollider ContainerArea { get; set; }
        [field: SerializeField]
        private float CollectInteraval { get; set; } = 1.0f;
        [field: SerializeField]
        private GameObject Obfuscator { get; set; }

        public Rigidbody Rigidbody { get; private set; }
        public PhotonView PhotonView { get; private set; }
        private HashSet<AttachableItem> StoredItems { get; set; } = new();
        private bool IsCollectRequired { get; set; } = true;

        protected virtual void Awake()
        {
            PhotonView = GetComponent<PhotonView>();
            Rigidbody = GetComponent<Rigidbody>();
        }

        protected virtual void Start()
        {
            if (Obfuscator != null)
            {
                Obfuscator.SetActive(IsOpened == false);
            }

            StartCoroutine(CollectCoroutine());
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            IsCollectRequired = true;
        }

        protected virtual void OnDestroy()
        {
            List<AttachableItem> itemsToDetach = new (StoredItems);

            for (int i = 0; i < itemsToDetach.Count; i++)
            {
                itemsToDetach[i].Detach();
            }
        }

        public void OnItemAttached(AttachableItem item)
        {
            StoredItems.Add(item);
            item.Interactable.InteractionStarted += OnStoredItemInteractionStart;

            if (IsOpened == false)
            {
                SetItemRenderersEnabled(item, false);
            }
        }

        public void OnItemDetached(AttachableItem item)
        {
            StoredItems.Remove(item);
            item.Interactable.InteractionStarted -= OnStoredItemInteractionStart;
            IsCollectRequired = true;

            if (IsOpened == false)
            {
                SetItemRenderersEnabled(item, true);
            }
        }

        private void OnStoredItemInteractionStart(InteractionInfo interaction)
        {
            AttachableItem itemToDetach = null;

            foreach (AttachableItem item in StoredItems)
            {
                if (item.Interactable == interaction.Interactable)
                {
                    itemToDetach = item;
                    break;
                }
            }

            itemToDetach.Detach();
        }

        private IEnumerator CollectCoroutine()
        {
            WaitForSeconds waitInterval = new WaitForSeconds(CollectInteraval);
            yield return new WaitUntil(() => PhotonNetwork.InRoom);

            while (true)
            {
                if (IsCollectRequired == true)
                {
                    IsCollectRequired = CollectObjects();
                }

                yield return waitInterval;
            }
        }

        private bool CollectObjects()
        {
            bool isRecollectRequired = false;

            Vector3 halfExtents = Vector3.Scale(ContainerArea.size * 0.5f, ContainerArea.transform.lossyScale);
            Collider[] overlapingColliders = Physics.OverlapBox(transform.TransformPoint(ContainerArea.center), halfExtents, ContainerArea.transform.rotation);

            for (int i = 0; i < overlapingColliders.Length; i++)
            {
                AttachableItem item = overlapingColliders[i].GetComponentInParent<AttachableItem>();

                if (item != null && StoredItems.Contains(item) == false)
                {
                    if (CanBeStored(item) == true)
                    {
                        Vector3 relativeItemPosition = transform.InverseTransformPoint(item.transform.position);
                        Quaternion relativeItemRotation = Quaternion.Inverse(transform.rotation) * item.transform.rotation;
                        item.Attach(this, relativeItemPosition, relativeItemRotation);
                    }
                    else
                    {
                        isRecollectRequired = true;
                    }
                }
            }

            return isRecollectRequired;
        }

        private bool CanBeStored(AttachableItem item)
        {
            return item.Rigidbody.IsSleeping() == true
                && item.PhotonView.IsMine == true;
        }

        private void SetItemRenderersEnabled(AttachableItem item, bool isEnabled)
        {
            Renderer[] renderers = item.GetComponentsInChildren<Renderer>(true);

            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].enabled = isEnabled;
            }
        }
    }
}
