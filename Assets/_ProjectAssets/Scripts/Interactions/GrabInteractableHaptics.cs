using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using Utilities;

namespace Defender.Interactions
{
    [RequireComponent(typeof(GrabInteractable), typeof(PhysicsData))]
    public class GrabInteractableHaptics : MonoBehaviour
    {
        private const float COOLDOWN = 0.2f;
        private const float BASE_DURATION = 0.1f;

        [field: SerializeField]
        private LayerMask LayerMask { get; set; } = ~0;
        [field: SerializeField]
        private float ImpactScale { get; set; } = 0.1f;

        private GrabInteractable GrabInteractable { get; set; }
        private PhysicsData PhysicsData { get; set; }

        private List<XRBaseController> Controllers { get; set; } = new();
        private float CooldownEndTime { get; set; }

        protected virtual void Awake()
        {
            GrabInteractable = GetComponent<GrabInteractable>();
            PhysicsData = GetComponent<PhysicsData>();
            ResetCooldown();
        }

        protected virtual void OnEnable()
        {
            GrabInteractable.InteractionStarted += OnInteractionStarted;
            GrabInteractable.InteractionEnded += OnInteractionEnded;
        }

        protected virtual void OnDisable()
        {
            GrabInteractable.InteractionStarted -= OnInteractionStarted;
            GrabInteractable.InteractionEnded -= OnInteractionEnded;
        }

        private void OnInteractionStarted(InteractionInfo interaction)
        {
            PhysicsData.CollisionEnter += HandleCollision;
            XRBaseController controller = interaction.Interactor.Controller;

            if (controller != null)
            {
                Controllers.Add(controller);
            }
        }

        private void OnInteractionEnded(InteractionInfo interaction)
        {
            PhysicsData.CollisionEnter -= HandleCollision;
            XRBaseController controller = interaction.Interactor.Controller;

            if (controller != null)
            {
                Controllers.Remove(controller);
            }
        }

        private void HandleCollision(Rigidbody rigidbody, Collision collision)
        {
            if (LayerMask.Contains(collision.gameObject.layer) == true)
            {
                ContactPoint contactPoint = collision.GetContact(0);
                Vector3 projectedVelocity = Vector3.Project(collision.relativeVelocity, contactPoint.normal);

                float impactStrength = projectedVelocity.magnitude * ImpactScale;
                float duration = BASE_DURATION * impactStrength;
                //Debug.Log(duration);

                if (Time.time >= CooldownEndTime)
                {
                    for (int i = 0; i < Controllers.Count; i++)
                    {
                        Controllers[i].SendHapticImpulse(Mathf.Clamp01(impactStrength), duration);
                    }

                    ResetCooldown();
                }
            }
        }

        private void ResetCooldown()
        {
            CooldownEndTime = Time.time + COOLDOWN;
        }
    }
}
