using Defender.Networking;
using Photon.Pun;
using Photon.Pun.Simple;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [DefaultExecutionOrder(-100)]
    public class OwnershipCapturer : MonoBehaviour
    {
        private const float AUTO_REMOVE_TIME = 1.0f;

        private Rigidbody Rigidbody { get; set; }
        private float LastNotSleepingTime { get; set; }

        public void EmulateCollision(GameObject gameObject)
        {
            RequestOwnership(gameObject);
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();

            if (Rigidbody == null)
            {
                DestroyImmediate(this);
            }
        }

        protected virtual void OnEnable()
        {
            LastNotSleepingTime = Time.time;
            ToggleRigidbodyIsKinematic();
        }

        // Hack to trigger enter and exit callbacks
        private void ToggleRigidbodyIsKinematic()
        {
            Rigidbody.isKinematic = !Rigidbody.isKinematic;
            Rigidbody.isKinematic = !Rigidbody.isKinematic;
        }

        protected virtual void OnCollisionEnter(Collision collision)
        {
            if (collision.rigidbody != null)
            {
                RequestOwnership(collision.rigidbody.gameObject);
            }
        }

        private void RequestOwnership(GameObject collidingGameObject)
        {
            // Correct IsKinematic value is later set by PhysicsControlManager (placed here to immiediatrly solve collision)
            collidingGameObject.GetComponent<PhysicsData>()?.SetRigidbodiesIsKinematic(false);
            collidingGameObject.GetComponent<SyncTransform>()?.ClearCachedFrames();
            collidingGameObject.GetComponentInParent<PhotonView>()?.RequestOwnership();
        }

        private void FixedUpdate()
        {
            if (Rigidbody.IsSleeping() == false)
            {
                LastNotSleepingTime = Time.time;
            }
            else if (Time.time > LastNotSleepingTime + AUTO_REMOVE_TIME)
            {
                enabled = false;
            }
        }
    }
}

