using Defender.Interactions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    public class CollisionEventsTrigger : MonoBehaviour
    {
        public PhysicsData PhysicsData { get; set; }
        private Rigidbody Rigidbody { get; set; }

        protected virtual void Awake()
        {
            PhysicsData = GetComponentInParent<PhysicsData>();
            Rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            PhysicsData.CollisionEnter.Invoke(Rigidbody, collision);
        }
    }
}

