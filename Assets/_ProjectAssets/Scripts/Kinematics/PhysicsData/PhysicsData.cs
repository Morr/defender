using Defender.Kinematics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Interactions
{
    [DefaultExecutionOrder(-1)]
    public class PhysicsData : MonoBehaviour
    {
        [field: SerializeField]
        private Collider[] ExcludedColliders { get; set; }

        public System.Action<Rigidbody, Collision> CollisionEnter { get; set; } = delegate { };
        public List<Rigidbody> Rigidbodies { get; private set; } = new();
        public List<Collider> Colliders { get; private set; } = new();

        public float CalculateCombinedMass()
        {
            float totalMass = 0.0f;

            for (int i = 0; i < Rigidbodies.Count; i++)
            {
                totalMass += Rigidbodies[i].mass;
            }

            return totalMass;
        }

        public Collider GetCloasestCollider(Vector3 point, out Vector3 cloasestPointOnCollider)
        {
            cloasestPointOnCollider = default;
            Collider cloasestCollider = null;
            float cloasestPointDistance = Mathf.Infinity;

            for (int i = 0; i < Colliders.Count; i++)
            {
                Collider collider = Colliders[i];
                Vector3 pointOnCollider = collider.ClosestPoint(point);
                float distance = Vector3.Distance(point, pointOnCollider);

                if (distance < cloasestPointDistance)
                {
                    cloasestPointOnCollider = pointOnCollider;
                    cloasestCollider = collider;
                }
            }

            return cloasestCollider;
        }

        public void SetRigidbodiesIsKinematic(bool isKinematic)
        {
            for (int i = 0; i < Rigidbodies.Count; i++)
            {
                Rigidbody rigidbody = Rigidbodies[i];
                rigidbody.isKinematic = isKinematic;

                if (isKinematic == false)
                {
                    rigidbody.WakeUp();
                }
            }
        }

        protected virtual void Awake()
        {
            CollectColliders();
            CollectRigidbodies();
            AddCollisionEventsTriggers();
        }

        private void CollectRigidbodies()
        {
            Rigidbodies.AddRange(GetComponentsInChildren<Rigidbody>(true));
        }

        private void CollectColliders()
        {
            Colliders.AddRange(GetComponentsInChildren<Collider>(true));

            for (int i = 0; i < ExcludedColliders.Length; i++)
            {
                Colliders.Remove(ExcludedColliders[i]);
            }
        }

        private void AddCollisionEventsTriggers()
        {
            for (int i = 0; i < Rigidbodies.Count; i++)
            {
                Rigidbodies[i].gameObject.AddComponent<CollisionEventsTrigger>();
            }
        }
    }
}
