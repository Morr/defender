using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    public class PhysicsLoopController : MonoBehaviour
    {
        private void Awake()
        {
            int targetFrameRate = Mathf.RoundToInt(1.0f / Time.fixedDeltaTime);
            SetTargetFrameRate(targetFrameRate);
            DisableAutoSimulation();
        }

        private void DisableAutoSimulation()
        {
            Physics.autoSimulation = false;
        }

        private void SetTargetFrameRate(int frameRate)
        {
            Debug.Log($"Setting target frame rate to: {frameRate}");
            Application.targetFrameRate = frameRate;
        }

        private void Update()
        {
            Physics.Simulate(Time.fixedDeltaTime);
        }
    }
}
