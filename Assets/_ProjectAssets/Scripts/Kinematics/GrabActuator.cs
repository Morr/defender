using Defender.Interactions;
using Photon.Pun;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    [RequireComponent(typeof(Rigidbody))]
    [DefaultExecutionOrder(5)]
    public class GrabActuator : MonoBehaviour, IPunObservable
    {
        private static Dictionary<Interactor, GrabActuator> InteractorsBindings { get; set; } = new();

        [field: SerializeField]
        private Transform TransformToFollow { get; set; }

        public Action<GrabActuator> JointBreak { get; set; } = delegate { };
        private GrabProfile CurrentProfile { get; set; }
        private ConfigurableJoint ActuatorJoint { get; set; }
        private Rigidbody Rigidbody { get; set; }
        private PhotonView PhotonView { get; set; }

        private int GrabbedObjectId { get; set; }
        private int GrabbedRigidbodyIndex { get; set; }
        private Vector3 LocalGrabPoint { get; set; }
        private Quaternion GrabRoatation { get; set; }

        public static GrabActuator GetActuator(Interactor interactor)
        {
            return InteractorsBindings[interactor];
        }

        public void Bind(Interactor interactor)
        {
            InteractorsBindings.Add(interactor, this);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(GrabbedObjectId);
                stream.SendNext(GrabbedRigidbodyIndex);
                stream.SendNext(LocalGrabPoint);
                stream.SendNext(GrabRoatation);
            }
            else
            {
                int newGrabbedObjectId = (int)stream.ReceiveNext();
                int grabbedRigidbodyIndex = (int)stream.ReceiveNext();
                Vector3 localGrabPoint = (Vector3)stream.ReceiveNext();
                Quaternion grabRotation = (Quaternion)stream.ReceiveNext();

                if (newGrabbedObjectId != GrabbedObjectId)
                {
                    GrabObject(newGrabbedObjectId, grabbedRigidbodyIndex, localGrabPoint, grabRotation);
                }
            }
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            PhotonView = GetComponent<PhotonView>();
        }

        protected virtual void OnJointBreak(float breakForce)
        {
            JointBreak.Invoke(this);

            if (PhotonView.IsMine == true)
            {
                GrabObject(-1, default, default, default);
            }
        }

        // FixedUpdate
        protected virtual void Update()
        {
            if (TransformToFollow != null)
            {
                UpdateActuatorPosition();
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

        public void GrabObject(int newGrabbedObjectId, int grabbedRigidbodyIndex, Vector3 localGrabPoint, Quaternion grabRotation)
        {
            GrabbedObjectId = newGrabbedObjectId;
            GrabbedRigidbodyIndex = grabbedRigidbodyIndex;
            LocalGrabPoint = localGrabPoint;
            GrabRoatation = grabRotation;

            if (newGrabbedObjectId >= 0)
            {
                PhotonView grabbedObjectView = PhotonView.Find(GrabbedObjectId);
                GrabInteractable grababInteractable = grabbedObjectView.GetComponent<GrabInteractable>();
                Rigidbody grabbedRigidbody = grababInteractable.PhysicsData.Rigidbodies[GrabbedRigidbodyIndex];
                CurrentProfile = grababInteractable.Profile;

                CreateJoint(grabbedRigidbody);
            }
            else
            {
                DestroyJoint();
            }
        }

        private void CreateJoint(Rigidbody grabbedRigidbody)
        {
            if (ActuatorJoint == null)
            {
                if (grabbedRigidbody != null)
                {
                    ActuatorJoint = gameObject.AddComponent<ConfigurableJoint>();
                    SetupJoint(grabbedRigidbody, LocalGrabPoint, GrabRoatation);
                }
            }
        }

        private void DestroyJoint()
        {
            Destroy(ActuatorJoint);
            ActuatorJoint = null;
        }

        private void SetupJoint(Rigidbody grabbedRigidbody, Vector3 localGrabPoint, Quaternion actuatorGrabRotation)
        {
            Quaternion currentRotation = transform.rotation;
            transform.rotation = actuatorGrabRotation;
            ActuatorJoint.autoConfigureConnectedAnchor = false;
            ActuatorJoint.connectedBody = grabbedRigidbody;
            ActuatorJoint.connectedAnchor = localGrabPoint;
            CurrentProfile.ApplyTo(ActuatorJoint);
            transform.rotation = currentRotation;
        }

        private void UpdateActuatorPosition()
        {
            Vector3 targetPosition = TransformToFollow.position;
            Quaternion targetRotation = TransformToFollow.rotation;

            if (ActuatorJoint != null)
            {
                targetPosition = Vector3.MoveTowards(Rigidbody.position, targetPosition, CurrentProfile.VelocityLimit * Time.fixedDeltaTime);
                targetRotation = Quaternion.RotateTowards(Rigidbody.rotation, targetRotation, CurrentProfile.AngularVelocityLimit * Time.fixedDeltaTime);
            }

            Rigidbody.MovePosition(targetPosition);
            Rigidbody.MoveRotation(targetRotation);
        }
    }
}

