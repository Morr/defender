using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    public class ObjectFragment : MonoBehaviour
    {
        [field: SerializeField]
        private float Mass { get; set; }
        [field: SerializeField]
        private float Lifetime { get; set; }
        [field: SerializeField]
        private float RandomImpulseStrength { get; set; }
        [field: SerializeField]
        private float RandomTorqueStrength { get; set; }

        private const string FRAGMENTS_LAYER_NAME = "ObjectsFragments";

        public void Release()
        {
            gameObject.layer = LayerMask.NameToLayer(FRAGMENTS_LAYER_NAME);
            transform.SetParent(null);

            Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
            rigidbody.AddForce(GetRandomVector3(0.0f, 1.0f) * RandomImpulseStrength, ForceMode.Impulse);
            rigidbody.AddTorque(GetRandomVector3(0.0f, 1.0f) * RandomTorqueStrength, ForceMode.Impulse);
            rigidbody.mass = Mass;

            StartCoroutine(DestructionCoroutine(Lifetime));
        }

        private IEnumerator DestructionCoroutine(float lifetime)
        {
            yield return new WaitForSeconds(lifetime);
            Destroy(gameObject);
        }

        public static Vector3 GetRandomVector3(float min, float max)
        {
            return new Vector3(Random.Range(min, max), Random.Range(min, max), Random.Range(min, max));
        }
    }
}
