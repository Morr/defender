using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    [RequireComponent(typeof(PhotonView))]
    public class DestructableObject : MonoBehaviour
    {
        [field: SerializeField]
        private float DestroyImpulseThreshold { get; set; }

        public Action OnBeforeDestroy { get; set; } = delegate { };
        private PhotonView PhotonView { get; set; }

        protected virtual void Awake()
        {
            PhotonView = GetComponent<PhotonView>();
        }

        protected virtual void OnCollisionEnter(Collision collision)
        {
            if (collision.impulse.magnitude > DestroyImpulseThreshold && PhotonView.IsMine == true)
            {
                PhotonView.RPC(nameof(DestroyRPC), RpcTarget.All);
            }
        }

        [PunRPC]
        private void DestroyRPC()
        {
            OnBeforeDestroy.Invoke();
            Destroy(gameObject);
        }

        public void OnDestroy()
        {
            if (gameObject.scene.isLoaded == true)
            {
                ObjectFragment[] fragments = GetComponentsInChildren<ObjectFragment>();

                for (int i = 0; i < fragments.Length; i++)
                {
                    fragments[i].Release();
                }
            }
        }
    }
}

