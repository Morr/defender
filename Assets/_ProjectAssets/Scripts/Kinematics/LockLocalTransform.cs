using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    [RequireComponent(typeof(Rigidbody))]
    public class LockLocalTransform : MonoBehaviour
    {
        private Rigidbody Rigidbody { get; set; }
        private Vector3 DefaultLocalPosition { get; set; }
        private Quaternion DefaultLocalRotation { get; set; }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            DefaultLocalPosition = transform.localPosition;
            DefaultLocalRotation = transform.localRotation;
        }

        private void LateUpdate()
        {
            if (transform.parent != null)
            {
                Rigidbody.MovePosition(transform.parent.TransformPoint(DefaultLocalPosition));
                Rigidbody.MoveRotation(transform.parent.rotation * DefaultLocalRotation);
            }
            else
            {
                Rigidbody.MovePosition(DefaultLocalPosition);
                Rigidbody.MoveRotation(DefaultLocalRotation);
            }
        }
    }
}
