﻿using Defender.Interactions;
using Defender.Networking;
using Photon.Pun;
using Photon.Pun.Simple;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Kinematics
{
    public class PhysicsControlManager : MonoBehaviourSingleton<PhysicsControlManager>
    {
        //TODO: Find better way to observe syncTransform.enabled value
        public void NotifyAttachStatusChanged(PhotonView photonView)
        {
            UpdateNetworkObjectPhysicsControl(photonView);
        }

        protected virtual void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine(InitializeObjectsPhysicsControlCoroutine());
            NetworkEventSystem.Instance.NetworkObjectSpawned += OnNetworkObjectSpawned;
            NetworkEventSystem.Instance.OwnershipTransfered += OnOwnershipTransfered;
            NetworkEventSystem.Instance.OwnershipTransferFailed += OnOwnershipTransferFailed;
        }

        protected virtual void OnDisable()
        {
            NetworkEventSystem.Instance.NetworkObjectSpawned -= OnNetworkObjectSpawned;
            NetworkEventSystem.Instance.OwnershipTransfered -= OnOwnershipTransfered;
            NetworkEventSystem.Instance.OwnershipTransferFailed -= OnOwnershipTransferFailed;
        }

        private void OnNetworkObjectSpawned(PhotonMessageInfo info)
        {
            UpdateNetworkObjectPhysicsControl(info.photonView);
        }

        public void OnOwnershipTransfered(PhotonView view, Player previousOwner)
        {
            UpdateNetworkObjectPhysicsControl(view);
        }

        public void OnOwnershipTransferFailed(PhotonView view, Player senderOfFailedRequest)
        {
            UpdateNetworkObjectPhysicsControl(view);
        }

        private IEnumerator InitializeObjectsPhysicsControlCoroutine()
        {
            yield return new WaitUntil(() => PhotonNetwork.InRoom == true);

            PhotonView[] sceneNetworkObjects = FindObjectsOfType<PhotonView>();

            for (int i = 0; i < sceneNetworkObjects.Length; i++)
            {
                UpdateNetworkObjectPhysicsControl(sceneNetworkObjects[i]);
            }
        }

        private void UpdateNetworkObjectPhysicsControl(PhotonView photonView)
        {
            PhysicsData physicsDataComponent = photonView.GetComponent<PhysicsData>();
            SyncTransform syncTransform = photonView.GetComponent<SyncTransform>();

            if (physicsDataComponent != null)
            {
                physicsDataComponent.SetRigidbodiesIsKinematic(photonView.IsMine == false && syncTransform.enabled == true);
            }
        }
    }
}
