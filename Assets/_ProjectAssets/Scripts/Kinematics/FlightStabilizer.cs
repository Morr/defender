using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Kinematics
{
    public class FlightStabilizer : MonoBehaviour
    {
        [field: SerializeField]
        private Rigidbody Rigidbody { get; set; }

        [field: SerializeField]
        private Vector3 FlightDirection { get; set; }
        [field: SerializeField]
        private float CenterOfMassOffset { get; set; }
        [field: SerializeField]
        private float ForceApplicationPointOffset { get; set; }

        [field: SerializeField]
        private float StabilizationSpeedThreshold { get; set; }
        [field: SerializeField]
        private float StabilizationStrength { get; set; } = 0.25f;

        private float SqrSpeedThreshold { get; set; }

        private void Start()
        {
            //Rigidbody.centerOfMass = FlightDirection * CenterOfMassOffset;
            SqrSpeedThreshold = Mathf.Pow(StabilizationSpeedThreshold, 2.0f);
        }

        private void FixedUpdate()
        {
            if (Rigidbody.velocity.sqrMagnitude >= SqrSpeedThreshold)
            {
                //Vector3 forceApplicationPoint = transform.TransformPoint(FlightDirection * ForceApplicationPointOffset);
                //Vector3 target = transform.position + (Rigidbody.velocity.normalized * ForceApplicationPointOffset);

                //Vector3 pointVelocityChange = (target - forceApplicationPoint)
                //    * Rigidbody.velocity.magnitude
                //    * StabilizationStrength;

                //Rigidbody.AddForceAtPosition(pointVelocityChange, forceApplicationPoint, ForceMode.VelocityChange);

                // Simplified version
                transform.forward = Rigidbody.velocity;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.white;
            Vector3 centerOfMass = transform.TransformPoint(FlightDirection * CenterOfMassOffset);
            Gizmos.DrawWireSphere(centerOfMass, 0.05f);

            Gizmos.color = Color.yellow;
            Vector3 forceApplicationPoint = transform.TransformPoint(FlightDirection * ForceApplicationPointOffset);
            Gizmos.DrawWireSphere(forceApplicationPoint, 0.05f);
        }
    }
}

