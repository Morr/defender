using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Locomotion
{
    public class PlayerSprint : MonoBehaviour
    {
        [field: SerializeField]
        private InputActionReference SprintAction { get; set; }
        [field: SerializeField]
        private float SprintSpeedMultiplier { get; set; } = 1.0f;
        [field: SerializeField]
        ActionBasedContinuousMoveProvider MoveProvider { get; set; }

        private float WalkSpeed { get; set; }
        
        protected virtual void Awake()
        {
            WalkSpeed = MoveProvider.moveSpeed;
            SprintAction.action.performed += ApplySprintSpeed;
            SprintAction.action.canceled += ApplyWalkSpeed;
        }

        protected virtual void OnDestroy()
        {
            SprintAction.action.performed -= ApplySprintSpeed;
            SprintAction.action.canceled -= ApplyWalkSpeed;
        }

        private void ApplySprintSpeed(InputAction.CallbackContext callbackContext)
        {
            MoveProvider.moveSpeed = WalkSpeed * SprintSpeedMultiplier;
        }

        private void ApplyWalkSpeed(InputAction.CallbackContext callbackContext)
        {
            MoveProvider.moveSpeed = WalkSpeed;
        }
    }
}
