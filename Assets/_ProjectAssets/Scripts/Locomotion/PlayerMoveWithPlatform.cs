using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Locomotion
{
    [RequireComponent(typeof(CharacterController))]
    [DefaultExecutionOrder(100)]
    public class PlayerMoveWithPlatform : MonoBehaviour
    {
        [field: SerializeField]
        private PlayerPlatformDetector PlatformDetector { get; set; }

        public Vector3 PlatformRelativeMotion { get; private set; }
        private CharacterController PlayerCharacterController { get; set; }
        private Transform CurrentPlatformTransform { get; set; } = null;
        private Vector3 PreviousWorldPosition { get; set; }
        private Vector3 PreviousLocalPosition { get; set; }

        protected virtual void Awake()
        {
            PlayerCharacterController = GetComponent<CharacterController>();
            PlatformDetector.PlatformChanged += OnPlatformChanged;
        }

        protected virtual void OnDestroy()
        {
            PlatformDetector.PlatformChanged -= OnPlatformChanged;
        }

        protected virtual void LateUpdate()
        {
            if (CurrentPlatformTransform != null)
            {
                MoveWithPlatform();
            }
            else
            {
                Vector3 currentWorldPosition = PlayerCharacterController.transform.TransformPoint(PlayerCharacterController.center);
                PlatformRelativeMotion = currentWorldPosition - PreviousWorldPosition;
                PreviousWorldPosition = currentWorldPosition;
            }
        }

        private void OnPlatformChanged(Transform platformTransform)
        {
            CurrentPlatformTransform = platformTransform;

            if (CurrentPlatformTransform != null)
            {
                Vector3 currentWorldPosition = PlayerCharacterController.transform.TransformPoint(PlayerCharacterController.center);
                PreviousWorldPosition = currentWorldPosition;
                PreviousLocalPosition = CurrentPlatformTransform.InverseTransformPoint(currentWorldPosition);
            }
        }

        private void MoveWithPlatform()
        {
            Vector3 currentWorldPosition = PlayerCharacterController.transform.TransformPoint(PlayerCharacterController.center);
            Vector3 characterMotion = currentWorldPosition - PreviousWorldPosition;

            Vector3 previous = CurrentPlatformTransform.TransformPoint(PreviousLocalPosition);
            Vector3 platformStandingPointMotion = previous - currentWorldPosition;
            platformStandingPointMotion += characterMotion;
            PlatformRelativeMotion = characterMotion;
            platformStandingPointMotion.y = 0;

            PlayerCharacterController.Move(platformStandingPointMotion);

            PreviousWorldPosition = PlayerCharacterController.transform.TransformPoint(PlayerCharacterController.center); ;
            PreviousLocalPosition = CurrentPlatformTransform.InverseTransformPoint(PreviousWorldPosition);
        }
    }
}
