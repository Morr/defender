using Defender.Interactions;
using Defender.Networking;
using Defender.Rigs;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NetworkPlayer = Defender.Networking.NetworkPlayer;

namespace Defender.Locomotion
{
    public class PlayerPlatformDetector : MonoBehaviour
    {
        [field: SerializeField]
        private LocalPlayer LocalPlayer { get; set; }
        [field: SerializeField]
        private float BackStep { get; set; } = 0.1f;
        [field: SerializeField]
        private float SpherecastMaxDistance { get; set; } = 1.0f;
        [field: SerializeField]
        private float SpherecastRadius { get; set; } = 0.2f;
        [field: SerializeField]
        private LayerMask PlatformLayerMask { get; set; }

        public System.Action<Transform> PlatformChanged { get; set; } = delegate { };
        public PhotonView CurrentPlatformPhotonView { get; private set; }
        public Transform CurrentPlatformTransform { get; private set; }

        protected virtual void FixedUpdate()
        {
            DetectPlatform();
        }

        private void DetectPlatform()
        {
            Vector3 origin = CalculateSpherecastOrigin();
            float distance = CalculateRaycastDistance();
            PhotonView newPlatformPhotonView = null;

            if (Physics.SphereCast(new Ray(origin, Vector3.down), SpherecastRadius, out RaycastHit hit, distance, PlatformLayerMask) == true)
            {
                newPlatformPhotonView = hit.transform.GetComponentInParent<PhotonView>();
            }

            if (newPlatformPhotonView != CurrentPlatformPhotonView)
            {
                CurrentPlatformPhotonView = newPlatformPhotonView;

                // Set LocalPlayer object parent
                CurrentPlatformTransform = CurrentPlatformPhotonView == null ? null : CurrentPlatformPhotonView.transform;
                NetworkPlayer networkPlayer = LocalPlayer.BoundNetworkPlayer;

                // Set NetworkPlayer object parent
                if (CurrentPlatformTransform != null)
                {
                    LocalPlayer.CharacterController.detectCollisions = false;
                    Vector3 localPosition = CurrentPlatformTransform.InverseTransformPoint(networkPlayer.transform.position);
                    Quaternion localRotation = Quaternion.Inverse(CurrentPlatformTransform.transform.rotation) * networkPlayer.transform.rotation;
                    NetwrokHierarchyManager.Instance.SetNetworkObjectParent(networkPlayer.PhotonView, CurrentPlatformPhotonView, localPosition, localRotation);
                }
                else
                {
                    LocalPlayer.CharacterController.detectCollisions = true;
                    NetwrokHierarchyManager.Instance.SetNetworkObjectParent(LocalPlayer.BoundNetworkPlayer.PhotonView, null, networkPlayer.transform.position, networkPlayer.transform.rotation);
                }

                PlatformChanged.Invoke(CurrentPlatformTransform);
            }
        }

        private Vector3 CalculateSpherecastOrigin()
        {
            Vector3 localOrigin = LocalPlayer.CharacterController.center 
                + Vector3.down * LocalPlayer.CharacterController.height * 0.5f
                + Vector3.up * BackStep;

            return LocalPlayer.transform.TransformPoint(localOrigin);
        }

        private float CalculateRaycastDistance()
        {
            return SpherecastMaxDistance + BackStep;
        }

        private void OnDrawGizmosSelected()
        {
            Vector3 begin = CalculateSpherecastOrigin();
            Vector3 end = begin + Vector3.down * CalculateRaycastDistance();
            float radius = SpherecastRadius;

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(end, radius);
            Gizmos.DrawLine(begin, end);
        }
    }
}
