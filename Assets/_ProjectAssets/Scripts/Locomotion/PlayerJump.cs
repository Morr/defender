using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Locomotion
{
    [DefaultExecutionOrder(-10)]
    public class PlayerJump : MonoBehaviour
    {
        [field: SerializeField]
        private InputActionReference JumpAction { get; set; }
        [field: SerializeField]
        private CharacterController CharacterController { get; set; }
        [field: SerializeField]
        private float JumpHeight { get; set; }

        private float VerticalVelocity { get; set; }

        protected virtual void Awake()
        {
            JumpAction.action.performed += Jump;
        }

        protected virtual void OnDestroy()
        {
            JumpAction.action.performed -= Jump;
        }

        private void Jump(InputAction.CallbackContext callbackContext)
        {
            Vector2 value = callbackContext.ReadValue<Vector2>();
            bool isJumpPressed = value.y > 0.5f;

            if (isJumpPressed == true && CharacterController.isGrounded == true)
            {
                VerticalVelocity = Mathf.Sqrt(JumpHeight * -2.0f * Physics.gravity.y);
            }
        }

        // Must not be done in FixedUpdate (parent movement problem)
        private void Update()
        {
            if (CharacterController.isGrounded == false)
            {
                VerticalVelocity += Physics.gravity.y * Time.deltaTime;
            }
            else
            {
                //VerticalVelocity = -0.1f;
            }

            CharacterController.Move(new Vector3(0, VerticalVelocity * Time.deltaTime, 0));
        }
    }
}
