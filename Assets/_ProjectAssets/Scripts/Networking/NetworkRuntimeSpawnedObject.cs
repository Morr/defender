using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Networking
{
    public class NetworkRuntimeSpawnedObject : MonoBehaviour, IPunInstantiateMagicCallback
    {
        public void OnPhotonInstantiate(PhotonMessageInfo spawnInfo)
        {
            NetworkEventSystem.Instance.NotifyNetworkObjectSpawned(spawnInfo);
        }
    }
}
