using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Networking
{
    public class NetworkManager : MonoBehaviourPunCallbacks
    {
        [field: SerializeField]
        private int SerializationRate { get; set; } = 10;
        [field: SerializeField]
        private string RoomName { get; set; }
        [field: SerializeField]
        private byte MaxPlayers { get; set; }

        protected virtual void Start()
        {
            PhotonNetwork.SerializationRate = SerializationRate;
            ConnectedToServer();
        }

        private void ConnectedToServer()
        {
            Debug.Log("Connecting to server");
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("Connected to server");
            base.OnConnectedToMaster();

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = MaxPlayers;
            roomOptions.IsVisible = true;
            roomOptions.IsOpen = true;

            PhotonNetwork.JoinOrCreateRoom(RoomName, roomOptions, TypedLobby.Default);
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("Room joined");
            base.OnJoinedRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log("Player joined the room");
            base.OnPlayerEnteredRoom(newPlayer);
        }
    }

}
