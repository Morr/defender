using Defender.Rigs;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Defender.Networking
{
    public class NetworkPlayerSpawner : MonoBehaviourPunCallbacks
    {
        [field: SerializeField]
        private string PlayerPrefabName { get; set; }
        [field: SerializeField]
        private LocalPlayer LocalPlayer { get; set; }

        private GameObject NetworkPlayerObject { get; set; }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            NetworkPlayerObject = PhotonNetwork.Instantiate(PlayerPrefabName, transform.position, transform.rotation);
            NetworkPlayer networkPlayer = NetworkPlayerObject.GetComponent<NetworkPlayer>();
            LocalPlayer.BindToNetworkPlayer(networkPlayer);
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            PhotonNetwork.Destroy(NetworkPlayerObject);
        }
    }
}
