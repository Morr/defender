using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Networking
{
    [DefaultExecutionOrder(-10)]
    public class NetworkEventSystem : MonoBehaviourSingleton<NetworkEventSystem>, IPunOwnershipCallbacks
    {
        public Action<PhotonMessageInfo> NetworkObjectSpawned { get; set; } = delegate { };
        public Action<PhotonView, Player> OwnershipRequested { get; set; } = delegate { };
        public Action<PhotonView, Player> OwnershipTransfered { get; set; } = delegate { };
        public Action<PhotonView, Player> OwnershipTransferFailed{ get; set; } = delegate { };

        public void NotifyNetworkObjectSpawned(PhotonMessageInfo spawnInfo)
        {
            NetworkObjectSpawned.Invoke(spawnInfo);
        }

        public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
        {
            OwnershipRequested.Invoke(targetView, requestingPlayer);
        }

        public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
        {
            OwnershipTransfered.Invoke(targetView, previousOwner);
        }

        public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
        {
            OwnershipTransferFailed.Invoke(targetView, senderOfFailedRequest);
        }

        protected virtual void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }

        protected virtual void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }
    }

}
