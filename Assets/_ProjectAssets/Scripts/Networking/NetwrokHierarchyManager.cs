using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Networking
{
    [RequireComponent(typeof(PhotonView))]
    public class NetwrokHierarchyManager : MonoBehaviourSingleton<NetwrokHierarchyManager>
    {
        PhotonView HierarchyManagerPhotonView { get; set; }

        public void SetNetworkObjectParent(PhotonView networkObjectView, PhotonView parentView, Vector3 localPosition, Quaternion localRotation)
        {
            int parentViewId = parentView == null ? -1 : parentView.ViewID;
            HierarchyManagerPhotonView.RPC(nameof(SetNetworokObjectParentRemote), RpcTarget.AllBuffered, networkObjectView.ViewID, parentViewId, localPosition, localRotation);
        }

        protected override void InitializeSingleton()
        {
            HierarchyManagerPhotonView = GetComponent<PhotonView>();
        }

        [PunRPC]
        private void SetNetworokObjectParentRemote(int networkObjectViewId, int parentViewId, Vector3 localPosition, Quaternion localRotation)
        {
            Transform networkObjectTransform = PhotonView.Find(networkObjectViewId).transform;
            Transform parentTransform = parentViewId >= 0 ? PhotonView.Find(parentViewId).transform : null;

            networkObjectTransform.SetParent(parentTransform);
            networkObjectTransform.localPosition = localPosition;
            networkObjectTransform.localRotation = localRotation;
        }
    }
}
