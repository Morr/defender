using Defender.Kinematics;
using Defender.Rigs;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Defender.Networking
{
    [RequireComponent(typeof(PhotonView))]
    public class NetworkPlayer : MonoBehaviour
    {
        [field: SerializeField]
        public Transform HeadTransform { get; set; }
        [field: SerializeField]
        public HandObjectsReferences LeftHandReferences { get; set; }
        [field: SerializeField]
        public HandObjectsReferences RightHandReferences { get; set; }
        
        public PhotonView PhotonView { get; private set; }

        protected virtual void Awake()
        {
            PhotonView = GetComponent<PhotonView>();
        }

        [System.Serializable]
        public struct HandObjectsReferences
        {
            [field: SerializeField]
            public Transform HandTransform { get; private set; }
            [field: SerializeField]
            public Animator HandAnimator { get; private set; }
            [field: SerializeField]
            public GrabActuator GrabActuator { get; private set; }
        }
    }
}
