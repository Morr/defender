using Defender.Networking;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Networking
{
    [DefaultExecutionOrder(10)]
    public class NetworkGizmosSpawner : MonoBehaviour
    {
        private const string PLAYER_TAG = "Player";

        protected virtual void OnEnable()
        {
            AddNetworkGizmos();
            NetworkEventSystem.Instance.NetworkObjectSpawned += OnNetworkObjectSpawned;
        }

        protected virtual void OnDisable()
        {
            RemoveNetworkGizmos();
            NetworkEventSystem.Instance.NetworkObjectSpawned -= OnNetworkObjectSpawned;
        }

        private void OnNetworkObjectSpawned(PhotonMessageInfo spawnInfo)
        {
            TryAddNetworkGizmoComponent(spawnInfo.photonView.gameObject);
        }
        
        private void AddNetworkGizmos()
        {
            PhotonView[] photonViews = FindObjectsOfType<PhotonView>();

            for (int i = 0; i < photonViews.Length; i++)
            {
                TryAddNetworkGizmoComponent(photonViews[i].gameObject);
            }
        }

        private void RemoveNetworkGizmos()
        {
            NetworkGizmo[] networkGizmos = FindObjectsOfType<NetworkGizmo>(true);

            for (int i = 0; i < networkGizmos.Length; i++)
            {
                Destroy(networkGizmos[i]);
            }
        }

        private bool TryAddNetworkGizmoComponent(GameObject targetObject)
        {
            if (targetObject.CompareTag(PLAYER_TAG) == false)
            {
                targetObject.AddComponent<NetworkGizmo>();
                return true;
            }

            return false;
        }
    }
}
