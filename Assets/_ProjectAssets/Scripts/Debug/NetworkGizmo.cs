using Defender.Interactions;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Defender.Networking
{
    [RequireComponent(typeof(PhotonView))]
    public class NetworkGizmo : MonoBehaviour
    {
        private const string GIZMO_PREFAB_NAME = "NetworkGizmo";
        private const float GIZMO_PADDING = 0.02f;


        private PhotonView PhotonView { get; set; }
        private GameObject GizmoObject { get; set; }
        private Renderer GizmoRenderer { get; set; }


        // Move to separate class ---------------------------------------
        private static GameObject GizmoPrefab { get; set; }
        private static Material DefaultGizmoMaterial { get; set; }
        private static Material[] ColoredGizmoMaterials { get; set; }
        private static bool IsPrefabLoaded { get; set; } = false;

        private void LoadGizmoPrefab()
        {
            GizmoPrefab = Resources.Load<GameObject>(GIZMO_PREFAB_NAME);
        }

        private void InitializeMaterials()
        {
            Color[] playersColors = new Color[]
            {
                Color.red,
                Color.green,
                Color.blue,
                Color.yellow,
                Color.magenta,
                Color.cyan
            };

            ColoredGizmoMaterials = new Material[playersColors.Length];
            DefaultGizmoMaterial = GizmoPrefab.GetComponent<Renderer>().sharedMaterial;

            for (int i = 0; i < playersColors.Length; i++)
            {
                Color gizmoColor = playersColors[i];
                gizmoColor.a = DefaultGizmoMaterial.color.a;

                Material coloredMaterial = new Material(DefaultGizmoMaterial);
                coloredMaterial.color = gizmoColor;
                ColoredGizmoMaterials[i] = coloredMaterial;
            }
        }
        // ----------------------------------------------------------------

        protected virtual void Awake()
        {
            if (IsPrefabLoaded == false)
            {
                LoadGizmoPrefab();
                InitializeMaterials();
                IsPrefabLoaded = true;
            }

            Initialize();
            StartCoroutine(UpdateGizmoPositionAndScaleCoroutine());
        }

        protected virtual void OnEnable()
        {
            NetworkEventSystem.Instance.OwnershipTransfered += OnOwnershipTransfered;
        }

        protected virtual void OnDisable()
        {
            NetworkEventSystem.Instance.OwnershipTransfered -= OnOwnershipTransfered;
        }

        protected virtual void OnDestroy()
        {
            Destroy(GizmoObject);
        }

        private void Initialize()
        {
            PhotonView = GetComponent<PhotonView>();
            GizmoObject = Instantiate(GizmoPrefab);
            GizmoRenderer = GizmoObject.GetComponent<Renderer>();
        }

        private void OnOwnershipTransfered(PhotonView photonView, Player previousOwner)
        {
            if (photonView == PhotonView)
            {
                GizmoRenderer.material = photonView.Owner.ActorNumber < 0 ? DefaultGizmoMaterial : ColoredGizmoMaterials[PhotonView.Owner.ActorNumber];
            }
        }

        private IEnumerator UpdateGizmoPositionAndScaleCoroutine()
        {
            UpdateGizmoPositionAndScale();

            if (GetComponentInChildren<SkinnedMeshRenderer>() != null)
            {
                while (true)
                {
                    yield return null;
                    UpdateGizmoPositionAndScale();
                }
            }
        }

        private void UpdateGizmoPositionAndScale()
        {
            Quaternion defaultObjectRotation = transform.rotation;
            transform.rotation = Quaternion.identity;
            GizmoObject.transform.SetParent(null);

            Bounds bounds = CalculateCombinedBoundingBox();

            if (bounds.size.sqrMagnitude > 0.0f)
            {
                bounds.size += Vector3.one * GIZMO_PADDING * 2.0f;
            }

            GizmoObject.transform.position = bounds.center;
            GizmoObject.transform.localScale = bounds.size;
            GizmoObject.transform.SetParent(transform);
            GizmoObject.transform.rotation = Quaternion.identity;
            transform.rotation = defaultObjectRotation;
        }

        private Bounds CalculateCombinedBoundingBox()
        {
            Renderer[] renderers = GetComponentsInChildren<Renderer>(false);
            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

            for (int i = 0; i < renderers.Length; i++)
            {
                Renderer renderer = renderers[i];

                if (i == 0)
                {
                    bounds = renderer.bounds;
                }
                else
                {
                    bounds.Encapsulate(renderer.bounds);
                }
            }

            return bounds;
        }
    }
}
