using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    public class BoneRotationSync : MonoBehaviour
    {
        [field: SerializeField]
        private Transform SourceBone { get; set; }

        private Quaternion DefaultLocalRotation { get; set; }
        private ConfigurableJoint Joint { get; set; }

        protected virtual void Awake()
        {
            Joint = GetComponent<ConfigurableJoint>();
            DefaultLocalRotation = SourceBone.localRotation;
        }

        protected virtual void Update()
        {
            Joint.targetRotation = Quaternion.Inverse(SourceBone.localRotation) * DefaultLocalRotation;
        }
    }
}

