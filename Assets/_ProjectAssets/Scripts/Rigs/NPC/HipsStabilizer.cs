using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    public class HipsStabilizer : MonoBehaviour
    {
        [field: SerializeField]
        public Vector3 TargetForward { get; set; } = Vector3.forward;
        [field: SerializeField]
        public Vector3 GuidingForce { get; set; } = Vector3.forward * 80.0f;
        [field: SerializeField]
        public float TargetGroundDistance { get; set; }
        [field: SerializeField]
        private Transform SourceBone { get; set; }

        private Rigidbody HipsRigidbody { get; set; }
        private Quaternion DefaultRotation { get; set; }

        protected virtual void Awake()
        {
            HipsRigidbody = GetComponent<Rigidbody>();
            DefaultRotation = transform.rotation;
        }

        protected virtual void Update()
        {
            ApproachTargetForwardRotation();
            ApplyGuidingForce();
            //CorrectHeight();
        }

        private void ApproachTargetForwardRotation()
        {
            //https://answers.unity.com/questions/48836/determining-the-torque-needed-to-rotate-an-object.html
            Vector3 currentForward = HipsRigidbody.transform.forward;
            Vector3 x = Vector3.Cross(currentForward, TargetForward);
            float theta = Mathf.Asin(x.magnitude);

            Vector3 w = x.normalized * theta / Time.fixedDeltaTime;
            Quaternion q = transform.rotation * HipsRigidbody.inertiaTensorRotation;
            Vector3 torque = q * Vector3.Scale(HipsRigidbody.inertiaTensor, (Quaternion.Inverse(q) * w));

            HipsRigidbody.AddTorque(torque * 10.0f, ForceMode.Impulse);
        }

        private void ApplyGuidingForce()
        {
            HipsRigidbody.AddForce(GuidingForce);
        }

        private void CorrectHeight()
        {
            float groundDistance = TargetGroundDistance;

            if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, 2.0f) == true) 
            {
                groundDistance = transform.position.y - hit.point.y;
            }

            float delta = TargetGroundDistance - groundDistance;
            HipsRigidbody.AddForce(delta * Vector3.up, ForceMode.VelocityChange);
        }
    }
}
