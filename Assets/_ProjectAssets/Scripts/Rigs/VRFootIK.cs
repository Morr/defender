using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    public class VRFootIK : MonoBehaviour
    {
        [field: SerializeField]
        private Animator CharacterAnimator { get; set; }

        [field: SerializeField]
        private LayerMask LayerMask { get; set; }
        [field: SerializeField]
        private float CapsuleCastRadius { get; set; } = 0.04f;
        [field: SerializeField]
        private float CapsuleCastStepUp { get; set; } = 1.0f;
        [field: SerializeField]
        private Vector3 FootOffset { get; set; }
        [field: SerializeField]
        private float FootLength { get; set; } = 0.2f;

        [field: SerializeField]
        private float SmoothFactor { get; set; } = 10.0f;
        private IKState PreviousLeftFootState { get; set; }
        private IKState PreviousRightFootState { get; set; }

        [field: SerializeField]
        private float ForwardMaxAngle { get; set; }
        [field: SerializeField]
        private float BackwardMaxAngle { get; set; }
        [field: SerializeField]
        private float SidsesMaxAngle { get; set; }

        private float ForwardLimit { get; set; }
        private float BackwardLimit { get; set; }
        private float SideLimit { get; set; }

        private struct IKState
        {
            public Vector3 Position;
            public Quaternion Rotation;
            public float Weight;

            public static IKState Lerp(IKState firstState, IKState secondState, float t)
            {
                return new IKState()
                {
                    Position = Vector3.Lerp(firstState.Position, secondState.Position, t),
                    Rotation = Quaternion.Lerp(firstState.Rotation, secondState.Rotation, t),
                    Weight = Mathf.Lerp(firstState.Weight, secondState.Weight, t)
                };
            }

            public void Apply(Animator animator, AvatarIKGoal goal)
            {
                animator.SetIKPosition(goal, Position);
                animator.SetIKRotation(goal, Rotation);
                animator.SetIKPositionWeight(goal, Weight);
                animator.SetIKRotationWeight(goal, Weight);
            }
        }

        protected virtual void Start()
        {
            Vector3 MaxForwardAngleVector = Quaternion.Euler(0.0f, 0.0f, ForwardMaxAngle * -1.0f) * Vector3.up;
            Vector3 MaxBackwardAngleVector = Quaternion.Euler(0.0f, 0.0f, BackwardMaxAngle) * Vector3.up;
            Vector3 MaxSideAngleVector = Quaternion.Euler(SidsesMaxAngle, 0.0f, 0.0f) * Vector3.up;
            ForwardLimit = MaxForwardAngleVector.x;
            BackwardLimit = MaxBackwardAngleVector.x;
            SideLimit = MaxSideAngleVector.z;
        }

        private void OnAnimatorIK(int layerIndex)
        {
            PreviousLeftFootState = ExecuteFootIKPass(AvatarIKGoal.LeftFoot, PreviousLeftFootState, 1.0f);
            PreviousRightFootState = ExecuteFootIKPass(AvatarIKGoal.RightFoot, PreviousRightFootState, 1.0f);
        }

        private IKState ExecuteFootIKPass(AvatarIKGoal goal, IKState previousState, float weight)
        {
            CalculateFootTransforms(goal, out Vector3 animationFootPosition, out Vector3 animationFootTipPosition, out Quaternion animationFootRotation);

            float raycastDistance = CapsuleCastStepUp + FootOffset.y - CapsuleCastRadius;
            Vector3 stepUpVector = Vector3.up * CapsuleCastStepUp;
            Vector3 heelSpherePosition = animationFootPosition + stepUpVector;
            Vector3 tipSpherePosition = animationFootTipPosition + stepUpVector + Vector3.down * 0.01f;

            bool isSurfaceHitted = Physics.CapsuleCast(heelSpherePosition, tipSpherePosition, CapsuleCastRadius, Vector3.down, out RaycastHit hit, raycastDistance, LayerMask, QueryTriggerInteraction.Ignore);
            
            float interpotaltionParameter = SmoothFactor * Time.deltaTime;
            IKState newState = previousState;

            if (isSurfaceHitted == true)
            {
                Vector3 limitedHitNormal = Quaternion.Inverse(transform.rotation) * hit.normal;
                limitedHitNormal.z = Mathf.Clamp(limitedHitNormal.z, BackwardLimit, ForwardLimit);
                limitedHitNormal.x = Mathf.Clamp(limitedHitNormal.x, SideLimit * -1.0f, SideLimit);
                limitedHitNormal = transform.rotation * limitedHitNormal;

                Plane hitPlane = new Plane(limitedHitNormal, hit.point);
                hitPlane.Raycast(new Ray(heelSpherePosition, Vector3.down), out float distance);

                Vector3 heelPlacementPosition = heelSpherePosition;
                heelPlacementPosition.y -= distance;

                newState = new IKState()
                {
                    Position = heelPlacementPosition + limitedHitNormal * FootOffset.y,
                    Rotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(transform.forward, limitedHitNormal), limitedHitNormal),
                    Weight = weight
                };
            }
            else
            {
                newState.Position = animationFootPosition;
                newState.Rotation = animationFootRotation;
                newState.Weight = weight;
            }

            newState = IKState.Lerp(previousState, newState, interpotaltionParameter);
            newState.Apply(CharacterAnimator, goal);
            return newState;
        }


        private void CalculateFootTransforms(AvatarIKGoal goal, out Vector3 animationFootPosition, out Vector3 animationFootTipPosition, out Quaternion animationFootRotation)
        {
            animationFootPosition = CharacterAnimator.GetIKPosition(goal);
            animationFootRotation = CharacterAnimator.GetIKRotation(goal);
            animationFootTipPosition = animationFootPosition
                + (animationFootRotation * Vector3.forward) * (FootLength - CapsuleCastRadius);
        }

        #if UNITY_EDITOR
        //protected virtual void OnDrawGizmos()
        //{
        //    if (Application.isPlaying == true)
        //    {
        //        Gizmos.color = Color.green;
        //        DrawFootRaycastGizmos(AvatarIKGoal.LeftFoot);
        //        DrawFootRaycastGizmos(AvatarIKGoal.RightFoot);

        //        void DrawFootRaycastGizmos(AvatarIKGoal goal)
        //        {
        //            CalculateFootTransforms(goal, out Vector3 footPosition, out Vector3 footTipPosition, out Quaternion footRotation);
        //            Gizmos.DrawWireSphere(footPosition, CapsuleCastRadius);
        //            Gizmos.DrawWireSphere(footTipPosition, CapsuleCastRadius);
        //        }
        //    }
        //}
        #endif
    }
}

