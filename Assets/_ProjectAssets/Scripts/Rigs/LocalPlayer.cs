using UnityEngine;
using UnityEngine.XR;
using Utilities;
using Defender.Kinematics;
using NetworkPlayer = Defender.Networking.NetworkPlayer;
using UnityEngine.InputSystem;
using Photon.Pun;
using Defender.Interactions;

namespace Defender.Rigs
{
    public class LocalPlayer : MonoBehaviourSingleton<LocalPlayer>
    {
        [field: SerializeField]
        public Transform HeadsetTransform { get; private set; }
        [field: SerializeField]
        public Transform LeftControllerTransform { get; private set; }
        [field: SerializeField]
        public Transform RightControllerTransform { get; private set; }
        [field: SerializeField]
        public CharacterController CharacterController { get; private set; }

        [field: SerializeField]
        private Interactor LeftHandInteractor { get; set; }
        [field: SerializeField]
        private Interactor RightHandInteractor { get; set; }
        [field: SerializeField]
        private HandInputActions LeftHandInputActions { get; set; }
        [field: SerializeField]
        private HandInputActions RightHandInputActions { get; set; }

        public NetworkPlayer BoundNetworkPlayer { get; private set; }

        public void BindToNetworkPlayer(NetworkPlayer networkPlayer)
        {
            BoundNetworkPlayer = networkPlayer;
            networkPlayer.LeftHandReferences.GrabActuator.Bind(LeftHandInteractor);
            networkPlayer.RightHandReferences.GrabActuator.Bind(RightHandInteractor);

            ItemSlot[] holsters = networkPlayer.GetComponentsInChildren<ItemSlot>();

            for (int i = 0; i < holsters.Length; i++)
            {
                holsters[i].RegisterIgnoredCollider(CharacterController);
            }
        }

        protected virtual void Update()
        {
            if (BoundNetworkPlayer != null)
            {
                ApplyStateToNetworkPlayer();
            }
        }

        private void ApplyStateToNetworkPlayer()
        {
            HeadsetTransform.ApplyValuesTo(BoundNetworkPlayer.HeadTransform);
            LeftControllerTransform.ApplyValuesTo(BoundNetworkPlayer.LeftHandReferences.HandTransform);
            RightControllerTransform.ApplyValuesTo(BoundNetworkPlayer.RightHandReferences.HandTransform);

            LeftHandInputActions.CopyValuesParametersTo(BoundNetworkPlayer.LeftHandReferences.HandAnimator);
            RightHandInputActions.CopyValuesParametersTo(BoundNetworkPlayer.RightHandReferences.HandAnimator);
        }

        [System.Serializable]
        private struct HandInputActions
        {
            [field: SerializeField]
            public InputActionReference TriggerValueAction { get; private set; }
            [field: SerializeField]
            public InputActionReference GrabValueAction { get; private set; }

            public void CopyValuesParametersTo(Animator handAnimator)
            {
                handAnimator.SetFloat("Trigger", TriggerValueAction.action.ReadValue<float>());
                handAnimator.SetFloat("Grip", GrabValueAction.action.ReadValue<float>());
            }
        }
    }
}
