using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    [DefaultExecutionOrder(10)]
    public class FollowXRDeviceTransform : MonoBehaviour
    {
        [field: SerializeField]
        private Transform DeviceTransform { get; set; }
        [field: SerializeField]
        private Transform FollowingTransform { get; set; }
        [field: SerializeField]
        private Vector3 PositionOffset { get; set; }
        [field: SerializeField]
        private Vector3 RotationOffset { get; set; }

        protected virtual void Update()
        {
            if (DeviceTransform == null)
            {
                return;
            }

            FollowingTransform.position = DeviceTransform.TransformPoint(PositionOffset);
            FollowingTransform.rotation = DeviceTransform.rotation * Quaternion.Euler(RotationOffset);
        }
    }
}

