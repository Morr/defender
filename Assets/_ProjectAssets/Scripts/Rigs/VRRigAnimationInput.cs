using Defender.Locomotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    public class VRRigAnimationInput : MonoBehaviour
    {
        [field: SerializeField]
        private Animator CharacterAnimator { get; set; }
        [field: SerializeField]
        private Transform HeadTargetTransform { get; set; }
        [field: SerializeField]
        private float MoveSpeedThreshold { get; set; } = 0.1f;
        [field: SerializeField]
        private float SmoothFactor { get; set; } = 1.0f;
        [field: SerializeField]
        private Transform CharacterTransform { get; set; }

        private Vector3 PreviousHeadPosition { get; set; }
        private Vector3 PreviousLocalHeadsetVelocity { get; set; }
        private Transform PreviousCharacterParentTransform { get; set; }

        protected virtual void Start()
        {
            PreviousHeadPosition = HeadTargetTransform.position;
        }

        private void LateUpdate()
        {
            Vector3 headPositionDelta = CalculateHeadPositionDelta();
            float interpolationParameter = SmoothFactor;

            Vector3 headsetVelocity = headPositionDelta / Time.deltaTime;
            headsetVelocity.y = 0.0f;

            Vector3 localHeadsetVelocity = transform.InverseTransformVector(headsetVelocity);


            Vector3 interpolatedLocalHeadsetVelocity = Vector3.Lerp(PreviousLocalHeadsetVelocity, localHeadsetVelocity, interpolationParameter);
            CharacterAnimator.SetFloat("Speed", interpolatedLocalHeadsetVelocity.magnitude);
            CharacterAnimator.SetBool("IsMoving", interpolatedLocalHeadsetVelocity.magnitude > MoveSpeedThreshold);
            CharacterAnimator.SetFloat("DirectionX", Mathf.Clamp(interpolatedLocalHeadsetVelocity.x, -1.0f, 1.0f));
            CharacterAnimator.SetFloat("DirectionY", Mathf.Clamp(interpolatedLocalHeadsetVelocity.z, -1.0f, 1.0f));
            PreviousLocalHeadsetVelocity = interpolatedLocalHeadsetVelocity;
        }

        private Vector3 CalculateHeadPositionDelta()
        {
            Vector3 headPosition = HeadTargetTransform.position;

            if (CharacterTransform.parent != null)
            {
                headPosition = CharacterTransform.parent.InverseTransformPoint(headPosition);
            }

            // Reset position delta when platform changes
            if (CharacterTransform.parent != PreviousCharacterParentTransform)
            {
                PreviousCharacterParentTransform = CharacterTransform.parent;
                PreviousHeadPosition = headPosition;
            }

            Vector3 result = headPosition - PreviousHeadPosition;
            PreviousHeadPosition = headPosition;
            return result;
        }
    }
}

