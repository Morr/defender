using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Rigs
{
    public class RigRotationFollow : MonoBehaviour
    {
        [field: SerializeField]
        private Transform HeadTarget { get; set; }
        [field: SerializeField]
        private Transform AvatarTransform { get; set; }

        [field: SerializeField]
        public Transform LeftHandTargetTransform { get; set; }
        [field: SerializeField]
        public Transform RightHandTargetTransform { get; set; }

        [field: SerializeField]
        private float Smoothness { get; set; } = 0.5f;
        [field: SerializeField, Range(0.0f, 1.0f)]
        private float LookDirectionWeight { get; set; }

        private Vector3 DefaultHeadBodyOffset { get; set; }

        protected virtual void Start()
        {
            DefaultHeadBodyOffset = AvatarTransform.position - HeadTarget.position;
        }

        protected virtual void Update()
        {
            AvatarTransform.position = HeadTarget.position + DefaultHeadBodyOffset;

            Vector3 lookForward = Vector3.ProjectOnPlane(HeadTarget.forward, Vector3.up).normalized;

            Vector3 handsAxis = LeftHandTargetTransform.position - RightHandTargetTransform.position;
            Vector3 handsAxisForward = Vector3.Cross(Vector3.up, handsAxis).normalized;

            Vector3 targetBodyForwardDirection = Vector3.Lerp(handsAxisForward, lookForward, LookDirectionWeight);
            AvatarTransform.forward = Vector3.Lerp(AvatarTransform.forward, targetBodyForwardDirection, Time.deltaTime * Smoothness);
        }
    }
}
