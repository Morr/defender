using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class FloorGenerator
    {
        public GameObject[] CreateFloorMeshes(FloorData floorData, List<List<int>> regions, List<List<Vector2>> cutouts, GameObject floorMeshPrefab)
        {
            GameObject[] floorMeshes = new GameObject[regions.Count];

            for (int i = 0; i < regions.Count; i++)
            {
                floorMeshes[i] = CreateFloorMesh(floorData, regions[i], cutouts, floorMeshPrefab);
            }

            return floorMeshes;
        }

        public GameObject[] CreateFloorObjects(FloorData floorData, out List<List<Vector2>> cutouts, IDictionary<string, GameObject> floorObjectsPrefabs)
        {
            GameObject[] floorObjects = new GameObject[floorData.FloorObjects.Count];
            cutouts = new();

            for (int i = 0; i < floorData.FloorObjects.Count; i++)
            {
                FloorObjectData floorObjectData = floorData.FloorObjects[i];
                GameObject floorObject = InstantiateFloorObject(floorData, floorObjectData, floorObjectsPrefabs);
                FloorObjectBase floorObjectComponent = floorObject.GetComponent<FloorObjectBase>();

                List<Vector2> cutout = floorObjectComponent.CalculateLocalCutout(floorObjectData.Width, floorObjectData.Length);
                float angle = Vector2.SignedAngle(Vector2.right, floorObjectData.RightDirection);
                GeometryUtilities.RotatePoints(cutout, angle);
                cutouts.Add(cutout);
                floorObjects[i] = floorObject;
            }

            return floorObjects;
        }

        private GameObject InstantiateFloorObject(FloorData floorData, FloorObjectData floorObjectData, IDictionary<string, GameObject> floorObjectsPrefabs)
        {
            GameObject floorObjectPrefab = floorObjectsPrefabs[floorObjectData.PrefabIdentifier];
            GameObject floorObject = GameObject.Instantiate(floorObjectPrefab);
            floorObject.transform.position = floorObjectData.Position + Vector3.up * floorData.Thickness;
            floorObject.transform.forward = floorObjectData.RightDirection.To3DSpace();
            floorObject.GetComponent<IWidthDynamic>()?.SetWidth(floorObjectData.Width);
            floorObject.GetComponent<ILengthDynamic>()?.SetLength(floorObjectData.Length);
            floorObject.GetComponent<IHeightDynamic>()?.SetHeight(floorData.Height);
            return floorObject;
        }

        private GameObject CreateFloorMesh(FloorData floorData, List<int> region, List<List<Vector2>> cutouts, GameObject prefab)
        {
            GameObject floorSurfaceObject = GameObject.Instantiate(prefab);

            Vector3[] regionPoints = new Vector3[region.Count];

            for (int i = 0; i < regionPoints.Length; i++)
            {
                regionPoints[i] = floorData.Points[region[i]];
            }

            List<Vector2> mainPolygon = GeometryUtilities.To2DSpace(regionPoints);
            List<List<Vector2>> polygons = new();
            polygons.Add(mainPolygon);

            for (int i = 0; i < cutouts.Count; i++)
            {
                polygons = PolygonCutter.CutOutConvexPolygon(polygons, cutouts[i]);
            }
            
            floorSurfaceObject.GetComponent<BuildingFloor>().SetData(polygons, floorData.Thickness);
            return floorSurfaceObject;
        }
    }
}

