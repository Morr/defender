using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class RoofGenerator
    {
        public GameObject CreateSimpleRoof(float segmentLength, float segmentWidth, float angle, GameObject surfacePrefab, GameObject gablePrefab)
        {
            float angleInRadians = Mathf.Deg2Rad * angle;
            float halfSegmentWidth = segmentWidth * 0.5f;
            float halfSegmentLength = segmentLength * 0.5f;

            float segmentHeight = halfSegmentWidth * Mathf.Tan(angleInRadians);
            float surfaceHeight = halfSegmentWidth / Mathf.Cos(angleInRadians);

            // Surfaces
            GameObject roofObject = new GameObject("Roof");
            float roofSurfaceLength = segmentLength;
            float roofSurfaceHeight = surfaceHeight;

            GameObject leftRoofSurface = GameObject.Instantiate(surfacePrefab);

            leftRoofSurface.transform.SetParent(roofObject.transform);
            leftRoofSurface.GetComponent<IHeightDynamic>()?.SetHeight(roofSurfaceHeight);
            leftRoofSurface.GetComponent<ILengthDynamic>()?.SetLength(roofSurfaceLength);

            // Set transform
            leftRoofSurface.transform.Rotate(90.0f - angle, 0.0f, 0.0f);
            leftRoofSurface.transform.localPosition += Vector3.left * halfSegmentLength;
            leftRoofSurface.transform.localPosition += Vector3.up * segmentHeight;

            GameObject rightRoofSurface = GameObject.Instantiate(leftRoofSurface);
            rightRoofSurface.transform.SetParent(roofObject.transform);
            rightRoofSurface.transform.RotateAround(Vector3.zero, Vector3.up, 180.0f);

            // Gables
            GameObject frontGable = GameObject.Instantiate(gablePrefab);
            frontGable.GetComponent<IHeightDynamic>()?.SetHeight(segmentHeight);
            frontGable.GetComponent<ILengthDynamic>()?.SetLength(segmentWidth);
            frontGable.transform.SetParent(roofObject.transform);
            frontGable.transform.localPosition += Vector3.left * halfSegmentLength;
            frontGable.transform.Rotate(0.0f, 90.0f, 0.0f);

            GameObject backGable = GameObject.Instantiate(frontGable);
            backGable.transform.SetParent(roofObject.transform);
            frontGable.transform.Rotate(0.0f, 180.0f, 0.0f);
            backGable.transform.localPosition = Vector3.right * halfSegmentLength;
            return roofObject;
        }
    }
}

