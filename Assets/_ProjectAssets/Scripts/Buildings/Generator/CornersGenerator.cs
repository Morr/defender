using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public class CornersGenerator
    {
        public List<GameObject> CreateCorners(FloorData floorData, List<(int, int)> wallsIndices, List<List<int>> regions, GameObject cornerPrefab)
        {
            List<GameObject> cornersObjects = new();
            List<RadialCutout>[] pointsAngleRestrictions = CalculatePointsRadialCutouts(floorData, wallsIndices, regions);

            for (int pointIndex = 0; pointIndex < floorData.Points.Count; pointIndex++)
            {
                List<RadialCutout> pointRadialCutouts = pointsAngleRestrictions[pointIndex];

                if (pointRadialCutouts != null)
                {
                    pointRadialCutouts.Sort((first, second) => first.CompareTo(second));
                    GameObject cornerObject = CreateCorner(floorData.Points[pointIndex], pointRadialCutouts, floorData.Height, cornerPrefab);
                    cornersObjects.Add(cornerObject);
                }
            }

            return cornersObjects;
        }

        private List<RadialCutout>[] CalculatePointsRadialCutouts(FloorData floorData, List<(int, int)> wallsIndices, List<List<int>> regions)
        {
            List<Vector3> points = floorData.Points;
            List<RadialCutout>[] pointsRadialCutouts = new List<RadialCutout>[points.Count];
            Dictionary<int, List<int>> indicesDictionary = GraphUtilities.CreateIndicesDictionary(wallsIndices);

            for (int regionIndex = 0; regionIndex < regions.Count; regionIndex++)
            {
                List<int> region = regions[regionIndex];

                for (int i = 0; i < region.Count; i++)
                {
                    int pointIndex = region[i];
                    int previousPointIndex = region[region.GetLoopedIndex(i - 1)];
                    int nextPointIndex = region[region.GetLoopedIndex(i + 1)];

                    List<int> pointIndices = indicesDictionary[pointIndex];
                    pointIndices.Remove(previousPointIndex);
                    pointIndices.Remove(nextPointIndex);

                    RadialCutout restriction = CalculatePointRadialCutout(points[previousPointIndex].To2DSpace(),
                        points[pointIndex].To2DSpace(),
                        points[nextPointIndex].To2DSpace());

                    if (pointsRadialCutouts[pointIndex] == null)
                    {
                        pointsRadialCutouts[pointIndex] = new();
                    }

                    List<RadialCutout> cuouts = pointsRadialCutouts[pointIndex];
                    bool isMergedToOtherRestriction = false;

                    // Restricted areas cannot overlap, but can touch other restrictions
                    for (int restrictionIndex = 0; restrictionIndex < cuouts.Count; restrictionIndex++)
                    {
                        RadialCutout existingRestriction = cuouts[restrictionIndex];

                        if (existingRestriction.TryMerge(restriction, out RadialCutout mergedRestriction) == true)
                        {
                            cuouts[restrictionIndex] = mergedRestriction;
                            isMergedToOtherRestriction = true;
                            break;
                        }
                    }

                    if (isMergedToOtherRestriction == false)
                    {
                        cuouts.Add(restriction);
                    }
                }
            }

            foreach (KeyValuePair<int, List<int>> pair in indicesDictionary)
            {
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    int otherPointIndex = pair.Value[i];
                    Vector2 otherPointVector = (points[otherPointIndex] - points[pair.Key]).To2DSpace();
                    RadialCutout restriction = new RadialCutout(otherPointVector, otherPointVector);

                    if (pointsRadialCutouts[pair.Key] == null)
                    {
                        pointsRadialCutouts[pair.Key] = new List<RadialCutout>();
                    }

                    pointsRadialCutouts[pair.Key].Add(restriction);
                }
            }

            return pointsRadialCutouts;
        }

        private RadialCutout CalculatePointRadialCutout(Vector2 previousPoint, Vector2 Point, Vector2 nextPoint)
        {
            Vector2 previousVector = previousPoint - Point;
            Vector2 nextVector = nextPoint - Point;
            return new RadialCutout(previousVector, nextVector);
        }

        private GameObject CreateCorner(Vector3 position, List<RadialCutout> radialCutouts, float height, GameObject cornerPrefab)
        {
            GameObject cornerObject = GameObject.Instantiate(cornerPrefab);
            cornerObject.transform.position = position;

            cornerObject.GetComponent<IRadialDynamic>()?.SetRadialSections(radialCutouts);
            cornerObject.GetComponent<IHeightDynamic>()?.SetHeight(height);
            return cornerObject;
        }
    }
}
