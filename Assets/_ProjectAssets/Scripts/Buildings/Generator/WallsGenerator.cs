using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class WallsGenerator
    {
        public GameObject CreateWall(ref FloorData floorData, WallData wall, float wallHeight, IDictionary<string, GameObject> wallObjectsPrefabs, GameObject scalableSectionPrefab)
        {
            Vector3 beginPoint = floorData.Points[wall.BeginPointIndex];
            Vector3 endPoint = floorData.Points[wall.EndPointIndex];
            Vector3 wallVector = endPoint - beginPoint;
            Vector3 wallDirection = wallVector.normalized;

            // Create sections (including implicit scalable sections)
            List<Vector2> scalableWallSections = new();
            float distanceTraversed = 0.0f;

            for (int i = 0; i < wall.WallObjects.Count; i++)
            {
                WallObjectData wallObjectData = wall.WallObjects[i];
                float halfObjectLength = wallObjectData.Length * 0.5f;
                float objectBegin = wallObjectData.WallPosition - halfObjectLength;
                float objectEnd = wallObjectData.WallPosition + halfObjectLength;

                scalableWallSections.Add(new Vector2(distanceTraversed, objectBegin));
                distanceTraversed = objectEnd;
            }

            scalableWallSections.Add(new Vector2(distanceTraversed, wallVector.magnitude));

            GameObject wallObject = new GameObject("Wall");

            for (int i = 0; i < scalableWallSections.Count; i++)
            {
                Vector2 section = scalableWallSections[i];
                float sectionLength = section.y - section.x;

                GameObject scalableSectionObject = GameObject.Instantiate(scalableSectionPrefab);
                scalableSectionObject.transform.right = wallDirection;
                scalableSectionObject.transform.position = beginPoint + wallDirection * section.x;

                scalableSectionObject.GetComponent<IHeightDynamic>()?.SetHeight(wallHeight);
                scalableSectionObject.GetComponent<ILengthDynamic>()?.SetLength(sectionLength);

                scalableSectionObject.transform.SetParent(wallObject.transform);
            }

            GameObject[] nonScalableSectionsObjects = InstantiateWallObject(wall, beginPoint, wallDirection, wallHeight, wallObjectsPrefabs);

            for (int i = 0; i < nonScalableSectionsObjects.Length; i++)
            {
                nonScalableSectionsObjects[i].transform.SetParent(wallObject.transform);
            }

            return wallObject;
        }

        private GameObject[] InstantiateWallObject(WallData wall, Vector3 beginNode, Vector3 wallDirection, float height, IDictionary<string, GameObject> wallObjectsPrefabs)
        {
            GameObject[] sectionsObjects = new GameObject[wall.WallObjects.Count];

            for (int i = 0; i < wall.WallObjects.Count; i++)
            {
                WallObjectData wallObjectData = wall.WallObjects[i];
                GameObject prefab = wallObjectsPrefabs[wallObjectData.PrefabIdentifier];
                float halfSectionLength = wallObjectData.Length * 0.5f;

                GameObject sectionObject = GameObject.Instantiate(prefab);
                sectionObject.GetComponent<IHeightDynamic>()?.SetHeight(height);

                sectionObject.transform.position = beginNode
                    + wallDirection * (wallObjectData.WallPosition - halfSectionLength);
                sectionObject.transform.right = wallDirection;

                sectionsObjects[i] = sectionObject;
            }

            return sectionsObjects;
        }
    }
}
