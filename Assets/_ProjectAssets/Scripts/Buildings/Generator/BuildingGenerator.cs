using RotaryHeart.Lib.SerializableDictionary;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public class BuildingGenerator : MonoBehaviourSingleton<BuildingGenerator>
    {
        [field: SerializeField]
        private GameObject CornerPrefab { get; set; }
        [field: SerializeField]
        private GameObject ScalableSectionPrefab { get; set; }

        [field: SerializeField]
        private GameObject WalkableFloorPrefab { get; set; }

        [field: SerializeField]
        private GameObject GablePrefab { get; set; }
        [field: SerializeField]
        private GameObject ScalableRoofSurfaceObjectPrefab { get; set; }
        [field: SerializeField]
        private float RoofAngle { get; set; }

        [field: SerializeField]
        private SerializableDictionaryBase<string, GameObject> SectionsPrefabs { get; set; }
        [field: SerializeField]
        private SerializableDictionaryBase<string, GameObject> FloorObjectsPrefabs { get; set; }


        private BuildingDataDeprecated BuildingData { get; set; }
        private GameObject BuildingObject { get; set; }

        private RegionsExtractor RegionsExtractor { get; set; } = new();
        private FloorGenerator FloorGenerator { get; set; } = new();
        private WallsGenerator WallsGenerator { get; set; } = new();
        private RoofGenerator RoofGenerator { get; set; } = new();
        private CornersGenerator CornersGenerator { get; set; } = new();

        private List<List<Vector2>> previousFloorCutouts = new();

        public GameObject CreateBuilding(BuildingDataDeprecated buildingData)
        {
            BuildingData = buildingData;
            BuildingObject = new GameObject(buildingData.Name);
            previousFloorCutouts.Clear();
            float placementHeight = 0.0f;

            for (int i = 0; i < buildingData.Floors.Count; i++)
            {
                CreateFloor(i, placementHeight);
                placementHeight += buildingData.Floors[i].Height;
            }

            //GameObject roofObject = RoofGenerator.CreateSimpleRoof(5.5f, 3.5f, RoofAngle, ScalableRoofSurfaceObjectPrefab, GablePrefab);
            //roofObject.transform.position = new Vector3(0.0f, placementHeight, 0.0f);
            //roofObject.transform.SetParent(BuildingObject.transform);

            return BuildingObject;
        }

        private void CreateFloor(int floorIndex, float placementHeight)
        {
            FloorData floorData = BuildingData.Floors[floorIndex];
            Transform floorParentTransform = new GameObject($"Floor{floorIndex}").transform;
            floorParentTransform.SetParent(BuildingObject.transform);

            List<(int, int)> wallsIndices = GetWallsIndices(floorData);
            List<List<int>> regions = RegionsExtractor.ExtractRegions(floorData.Points, wallsIndices);

            CreateFloorMeshesAndObjects(floorData, regions, floorParentTransform);
            CreateWalls(floorData, floorParentTransform);
            CreateCorners(floorData, wallsIndices, regions, floorParentTransform);

            floorParentTransform.transform.position += Vector3.up * placementHeight;
        }

        private void CreateFloorMeshesAndObjects(FloorData floorData, List<List<int>> regions, Transform floorParentTransform)
        {
            GameObject[] floorMeshes = FloorGenerator.CreateFloorMeshes(floorData, regions, previousFloorCutouts, WalkableFloorPrefab);
            floorMeshes.SetTransformsParent(floorParentTransform);
            GameObject[] floorObjects = FloorGenerator.CreateFloorObjects(floorData, out previousFloorCutouts, FloorObjectsPrefabs);
            floorObjects.SetTransformsParent(floorParentTransform);
        }

        private List<(int, int)> GetWallsIndices(FloorData floorData)
        {
            List<(int, int)> wallsIndices = new();

            for (int i = 0; i < floorData.Walls.Count; i++)
            {
                WallData wallData = floorData.Walls[i];
                wallsIndices.Add((wallData.BeginPointIndex, wallData.EndPointIndex));
            }

            return wallsIndices;
        }

        private void CreateWalls(FloorData floorData, Transform floorParentTransform)
        {
            for (int i = 0; i < floorData.Walls.Count; i++)
            {
                WallData wall = floorData.Walls[i];
                GameObject wallObject = WallsGenerator.CreateWall(ref floorData, wall, floorData.Height, SectionsPrefabs, ScalableSectionPrefab);
                wallObject.transform.SetParent(floorParentTransform);
            }
        }

        private void CreateCorners(FloorData floorData, List<(int, int)> wallsIndices, List<List<int>> regions, Transform floorParentTransform)
        {
            List<GameObject> cornersObjects = CornersGenerator.CreateCorners(floorData, wallsIndices, regions, CornerPrefab);
            cornersObjects.SetTransformsParent(floorParentTransform);
        }
    }
}
