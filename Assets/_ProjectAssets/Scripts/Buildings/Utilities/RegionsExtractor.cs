using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class RegionsExtractor
    {
        private static Dictionary<int, List<int>> IndicesDictionary = new();
        private static List<Vector3> Points;

        public List<List<int>> ExtractRegions(List<Vector3> points, List<(int, int)> indices)
        {
            List<List<int>> regions = new();
            Points = points;
            IndicesDictionary = GraphUtilities.CreateIndicesDictionary(indices);
            DiscardDetachedPoints();

            while (TryGetFirstAvailableIndicesPair(out (int, int) pair) == true)
            {
                int regionStartIndex = pair.Item1;
                int currentPointIndex = pair.Item2;
                int previousPointIndex = regionStartIndex;

                List<int> region = new();
                bool isRegionClosed = true;
                region.Add(currentPointIndex);
                DiscardIndicesPair((previousPointIndex, currentPointIndex));

                while (currentPointIndex != regionStartIndex)
                {
                    if (TryGetNextPointIndex(previousPointIndex, currentPointIndex, out int nextPointIndex))
                    {
                        DiscardIndicesPair((currentPointIndex, nextPointIndex));
                        region.Add(nextPointIndex);
                        previousPointIndex = currentPointIndex;
                        currentPointIndex = nextPointIndex;
                    }
                    else
                    {
                        isRegionClosed = false;
                        break;
                    }
                }

                if (isRegionClosed == true)
                {
                    regions.Add(region);
                }
            }

            IndicesDictionary.Clear();
            RemoveCounterClockwiseRegions(regions);
            return regions;
        }


        private void DiscardDetachedPoints()
        {
            Stack<int> stack = new(IndicesDictionary.Keys);

            while(stack.Count > 0)
            {
                int pointIndex = stack.Pop();

                if (IndicesDictionary.TryGetValue(pointIndex, out List<int> pointIndices) == true)
                {
                    if (pointIndices.Count <= 1)
                    {
                        if (pointIndices.Count == 1)
                        {
                            int connectedPointIndex = pointIndices[0];
                            IndicesDictionary[connectedPointIndex].Remove(pointIndex);
                            stack.Push(connectedPointIndex);
                        }

                        IndicesDictionary.Remove(pointIndex);
                    }
                }
            }
        }

        private void RemoveCounterClockwiseRegions(List<List<int>> regions)
        {
            List<float> regionSignedAreas = new();

            for (int regionIndex = 0; regionIndex < regions.Count; regionIndex++)
            {
                List<int> regionIndices = regions[regionIndex];
                List<Vector2> regionPoints = new();

                for (int i = 0; i < regionIndices.Count; i++)
                {
                    Vector3 point = Points[regionIndices[i]];
                    regionPoints.Add(point.To2DSpace());
                }

                float signedArea = GeometryUtilities.CalculateSignedArea(regionPoints);
                regionSignedAreas.Add(signedArea);
            }

            for (int i = regionSignedAreas.Count - 1; i >= 0; i--)
            {
                if (regionSignedAreas[i] > 0.0f)
                {
                    regions.RemoveAt(i);
                }
            }
        }

        private void DiscardIndicesPair((int, int) pair)
        {
            List<int> indicesList = IndicesDictionary[pair.Item1];
            indicesList.Remove(pair.Item2);

            if (indicesList.Count == 0)
            {
                IndicesDictionary.Remove(pair.Item1);
            }
        }

        private bool TryGetFirstAvailableIndicesPair(out (int, int) pair)
        {
            foreach (KeyValuePair<int, List<int>> keyValuePair in IndicesDictionary)
            {
                pair = (keyValuePair.Key, keyValuePair.Value[0]);
                return true;
            }

            pair = default;
            return false;
        }

        private bool TryGetNextPointIndex(int previousIndex, int currentIndex, out int nextPointIndex)
        {
            Vector3 currentPoint = Points[currentIndex];
            Vector3 currentVector = currentPoint - Points[previousIndex];

            nextPointIndex = default;
            bool isNextPointFound = false;
            float minAngle = Mathf.NegativeInfinity;
            List<int> currentPointIndices = IndicesDictionary[currentIndex];

            for (int i = 0; i < currentPointIndices.Count; i++)
            {
                int index = currentPointIndices[i];

                // Prevent going back
                if (index == previousIndex)
                {
                    continue;
                }

                Vector3 nextPoint = Points[index];
                Vector3 nextVector = nextPoint - currentPoint;
                float signedAngle = Vector3.SignedAngle(currentVector, nextVector, Vector3.up);

                if (signedAngle > minAngle)
                {
                    minAngle = signedAngle;
                    nextPointIndex = index;
                    isNextPointFound = true;
                }
            }

            return isNextPointFound;
        }
    }
}
