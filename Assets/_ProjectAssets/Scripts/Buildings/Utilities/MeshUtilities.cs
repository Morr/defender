using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public static class MeshUtilities
    {
        public static void ReverseIndices(int[] indices)
        {
            for (int i = 0; i < indices.Length; i += 3)
            {
                (indices[i], indices[i + 2]) = (indices[i + 2], indices[i]);
            }
        }

        public static void OffsetIndices(int[] indices, int vertexOffset)
        {
            for (int i = 0; i < indices.Length; i++)
            {
                indices[i] += vertexOffset;
            }
        }

        public static Mesh CombineMeshes(Mesh[] meshes, bool mergeSubMeshes = false)
        {
            CombineInstance[] combineInstances = new CombineInstance[meshes.Length];

            for (int i = 0; i < meshes.Length; i++)
            {
                CombineInstance combineInstance = new CombineInstance();
                combineInstance.mesh = meshes[i];
                combineInstance.transform = Matrix4x4.identity;
                combineInstances[i] = combineInstance;
            }

            Mesh combinedMesh = new Mesh();
            combinedMesh.CombineMeshes(combineInstances, mergeSubMeshes);
            return combinedMesh;
        }

        public static MeshGenerationData[] CreatePrism(Vector2[] polygon, float thickness)
        {
            MeshGenerationData surfacesMeshData = new MeshGenerationData();
            surfacesMeshData.AddPolygon(polygon, polygon, 0.0f, true);
            surfacesMeshData.AddPolygon(polygon, polygon, thickness, false);

            MeshGenerationData sidesMeshData = new MeshGenerationData();
            Vector3[] quadVertices = new Vector3[4];
            Vector2[] quadUVs = new Vector2[4];
            float distance = 0.0f;

            for (int i = 0; i < polygon.Length; i++)
            {
                Vector3 current = polygon[i].To3DSpace();
                int nextIndex = polygon.GetLoopedIndex(i + 1);
                Vector3 next = polygon[nextIndex].To3DSpace();
                quadVertices[0] = current;
                quadVertices[1] = next;
                quadVertices[2] = next + Vector3.up * thickness;
                quadVertices[3] = current + Vector3.up * thickness;

                float edgeLength = Vector3.Distance(quadVertices[0], quadVertices[1]);
                quadUVs[0] = new Vector2(distance, 0.0f);
                quadUVs[1] = new Vector2(distance + edgeLength, 0.0f);
                quadUVs[2] = new Vector2(distance + edgeLength, thickness);
                quadUVs[3] = new Vector2(distance, thickness);

                sidesMeshData.AddQuad(quadVertices, quadUVs);
                distance += edgeLength;
            }

            return new MeshGenerationData[] { surfacesMeshData, sidesMeshData };
        }
    }
}
