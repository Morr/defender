using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public static class PolygonCutter
    {
        public static List<List<Vector2>> CutOutConvexPolygon(List<List<Vector2>> polygons, List<Vector2> cutout)
        {
            List<List<Vector2>> result = new();

            for (int i = 0; i < polygons.Count; i++)
            {
                result.AddRange(CutOutConvexPolygon(polygons[i], cutout));
            }

            return result;
        }

        public static List<List<Vector2>> CutOutConvexPolygon(List<Vector2> polygon, List<Vector2> cutout)
        {
            List<List<Vector2>> result = new();
            List<List<Vector2>> tmp = new();
            tmp.Add(polygon);

            for (int i = 0; i < cutout.Count; i++)
            {
                int nextIndex = cutout.GetLoopedIndex(i + 1);
                Vector2 origin = cutout[i];
                Vector2 direction = cutout[nextIndex] - origin;
                Ray2D ray = new Ray2D(origin, direction);

                CutPolygons(tmp, ray, out List<List<Vector2>> leftSidePolygons, out List<List<Vector2>> rightSidePolygons);
                result.AddRange(leftSidePolygons);
                tmp = rightSidePolygons;
            }

            return result;
        }

        public static void CutPolygons(List<List<Vector2>> polygons, Ray2D ray, out List<List<Vector2>> leftSidePolygons, out List<List<Vector2>> rightSidePolygons)
        {
            leftSidePolygons = new();
            rightSidePolygons = new();

            for (int i = 0; i < polygons.Count; i++)
            {
                CutPolygon(polygons[i], ray, out List<List<Vector2>> leftSide, out List<List<Vector2>> rightSide);
                leftSidePolygons.AddRange(leftSide);
                rightSidePolygons.AddRange(rightSide);
            }
        }

        public static void CutPolygon(List<Vector2> polygon, Ray2D ray, out List<List<Vector2>> leftSidePolygons, out List<List<Vector2>> rightSidePolygons)
        {
            List<Vector2> transformedPolygon = new List<Vector2>(polygon);
            TemporaryTransform temporaryTransform = CalculateTemporaryTransformation(ray);
            temporaryTransform.Apply(transformedPolygon);

            List<Vector2> leftCombinedPolygon = new List<Vector2>(); // positive y points
            List<Vector2> rightCombinedPolygon = new List<Vector2>(); // negative y points

            for (int i = 0; i < transformedPolygon.Count; i++)
            {
                int nextVertexIndex = transformedPolygon.GetLoopedIndex(i + 1);
                Vector2 beginPoint = transformedPolygon[i];
                Vector2 endPoint = transformedPolygon[nextVertexIndex];

                if (beginPoint.y > 0.0f)
                {
                    leftCombinedPolygon.Add(beginPoint);
                }
                else
                {
                    rightCombinedPolygon.Add(beginPoint);
                }

                if (Mathf.Sign(endPoint.y) != Mathf.Sign(beginPoint.y))
                {
                    Vector2 crossingPoint = GetCrossingPoint(beginPoint, endPoint);
                    leftCombinedPolygon.Add(crossingPoint);
                    rightCombinedPolygon.Add(crossingPoint);
                }
            }

            FlipPointsSigns(rightCombinedPolygon);
            leftSidePolygons = SplitCombinedPolygon(leftCombinedPolygon);
            rightSidePolygons = SplitCombinedPolygon(rightCombinedPolygon);

            for (int i = 0; i < rightSidePolygons.Count; i++)
            {
                FlipPointsSigns(rightSidePolygons[i]);
            }

            temporaryTransform.Revert(leftSidePolygons);
            temporaryTransform.Revert(rightSidePolygons);
        }

        private static TemporaryTransform CalculateTemporaryTransformation(Ray2D ray)
        {
            Vector2 translation = ray.origin * -1.0f;
            float rotation = Vector2.SignedAngle(ray.direction, Vector2.right);
            return new TemporaryTransform(translation, rotation);
        }

        private static Vector2 GetCrossingPoint(Vector2 beginPoint, Vector2 endPoint)
        {
            float absoluteBeginPointY = Mathf.Abs(beginPoint.y);
            float absoluteEndPointY = Mathf.Abs(endPoint.y);
            float factor = absoluteBeginPointY / (absoluteBeginPointY + absoluteEndPointY);
            Vector2 crossingPoint = beginPoint + (endPoint - beginPoint) * factor;
            crossingPoint.y = 0.0f;
            return crossingPoint;
        }

        private static List<List<Vector2>> SplitCombinedPolygon(List<Vector2> combinedPolygon)
        {
            List<Vector2> currentPolygon = new();
            List<List<Vector2>> polygons = new();

            int firstIndex = GetLeftMostIndex(combinedPolygon);
            combinedPolygon.Rewind(firstIndex);

            for (int i = 0; i < combinedPolygon.Count; i++)
            {
                Vector2 point = combinedPolygon[i];
                currentPolygon.Add(point);

                if (point.y == 0.0f)
                {
                    if (currentPolygon.Count > 1)
                    {
                        polygons.Add(currentPolygon);
                        currentPolygon = new();

                        // Check if begin/end points overlap
                        int nextPointIndex = combinedPolygon.GetLoopedIndex(i + 1);
                        Vector2 nextPoint = combinedPolygon[nextPointIndex];

                        if (nextPoint.y > 0.0f)
                        {
                            currentPolygon.Add(point);
                        }
                    }
                }
            }

            // Polygon not intersecting with line
            if (polygons.Count == 0)
            {
                polygons.Add(combinedPolygon);
            }
            else
            {
                MergePolygons(polygons);
            }

            return polygons;
        }

        private static void MergePolygons(List<List<Vector2>> polygons)
        {
            List<PolygonWithCutouts> nodes = new();

            for (int polygonIndex = 0; polygonIndex < polygons.Count; polygonIndex++)
            {
                List<Vector2> polygon = polygons[polygonIndex];
                float begin = polygon[0].x;
                float end = polygon[polygon.Count - 1].x;

                // Cutout are always listed after polygons
                if (end > begin) 
                {
                    nodes.Add(new PolygonWithCutouts(polygon, begin, end));
                }
                else
                {
                    float minSpan = Mathf.Infinity;
                    PolygonWithCutouts parentNode = null;

                    for (int nodeIndex = 0; nodeIndex < nodes.Count; nodeIndex++)
                    {
                        PolygonWithCutouts node = nodes[nodeIndex];

                        if (node.IsContaining(begin) == true && node.Span < minSpan)
                        {
                            parentNode = node;
                            minSpan = node.Span;
                        }
                    }

                    parentNode.Cutouts.Add(polygon);
                }
            }

            polygons.Clear();

            for (int nodeIndex = 0; nodeIndex < nodes.Count; nodeIndex++)
            {
                PolygonWithCutouts node = nodes[nodeIndex];
                node.MergeCutouts();
                polygons.Add(node.Points);
            }
        }

        private static int GetLeftMostIndex(List<Vector2> polygon)
        {
            int leftMostIndex = 0;
            float min = Mathf.Infinity;

            for (int i = 0; i < polygon.Count; i++)
            {
                Vector2 point = polygon[i];

                if (point.y == 0.0f)
                {
                    if (point.x < min)
                    {
                        leftMostIndex = i;
                        min = point.x;
                    }
                }
            }

            return leftMostIndex;
        }

        private static void FlipPointsSigns(List<Vector2> points)
        {
            for (int i = 0; i < points.Count; i++)
            {
                points[i] *= -1.0f;
            }
        }

        private struct TemporaryTransform
        {
            public Vector2 Translation;
            public float EulerRotation;

            public TemporaryTransform(Vector2 translation, float rotation)
            {
                Translation = translation;
                EulerRotation = rotation;
            }

            public void Apply(List<Vector2> polygon)
            {
                GeometryUtilities.TranslatePoints(polygon, Translation);
                GeometryUtilities.RotatePoints(polygon, EulerRotation);
            }

            public void Revert(List<List<Vector2>> polygons)
            {
                for (int i = 0; i < polygons.Count; i++)
                {
                    List<Vector2> polygon = polygons[i];
                    GeometryUtilities.RotatePoints(polygon, EulerRotation * -1.0f);
                    GeometryUtilities.TranslatePoints(polygon, Translation * -1.0f);
                }
            }
        }

        private class PolygonWithCutouts
        {
            public List<Vector2> Points;
            public List<List<Vector2>> Cutouts = new();
            public float Begin;
            public float End;
            public float Span;

            public PolygonWithCutouts(List<Vector2> polygon, float begin, float end)
            {
                Points = polygon;
                Begin = begin;
                End = end;
                Span = end - begin;
            }

            public bool IsContaining(float x)
            {
                return (x > Begin) && (x < End);
            }

            public void MergeCutouts()
            {
                Cutouts.Sort((firstCutout, secondCutout) => secondCutout[0].x.CompareTo(firstCutout[0].x));

                for (int i = 0; i < Cutouts.Count; i++)
                {
                    Points.AddRange(Cutouts[i]);
                }

                Cutouts.Clear();
            }
        }
    }
}

