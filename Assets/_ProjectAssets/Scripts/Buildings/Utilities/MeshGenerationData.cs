﻿using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class MeshGenerationData
    {
        public List<Vector3> Vertices = new();
        public List<Vector2> UVs = new();
        public List<int> Indices = new();

        public void AddMesh(MeshGenerationData meshData)
        {
            int vertexOffset = Vertices.Count;
            Vertices.AddRange(meshData.Vertices);
            UVs.AddRange(meshData.UVs);

            for (int i = 0; i < meshData.Indices.Count; i++)
            {
                Indices.Add(meshData.Indices[i] + vertexOffset);
            }
        }

        public void AddPolygon(Vector2[] polygon, Vector2[] uvs, float placementHeight, bool isFlipped = false)
        {
            Vector3[] vertices = new Vector3[polygon.Length];

            for (int i = 0; i < polygon.Length; i++)
            {
                Vector2 polygonVertex = polygon[i];
                vertices[i] = new Vector3(polygonVertex.x, placementHeight, polygonVertex.y);
            };

            Triangulator triangulator = new Triangulator(polygon);
            int[] indices = triangulator.Triangulate();
            MeshUtilities.OffsetIndices(indices, Vertices.Count);

            if (isFlipped == true)
            {
                MeshUtilities.ReverseIndices(indices);
            }

            Vertices.AddRange(vertices);
            UVs.AddRange(uvs);
            Indices.AddRange(indices);
        }

        public void AddQuad(Vector3[] vertices, Vector2[] uvs, bool isFlipped = false)
        {
            int[] indices = new int[]
            {
                0, 1, 2, 2, 3, 0
            };

            MeshUtilities.OffsetIndices(indices, Vertices.Count);

            if (isFlipped == true)
            {
                MeshUtilities.ReverseIndices(indices);
            }

            Indices.AddRange(indices);
            Vertices.AddRange(vertices);
            UVs.AddRange(uvs);
        }

        public Mesh CreateMesh(MeshTopology meshTopology = MeshTopology.Triangles)
        {
            Mesh mesh = new Mesh();
            mesh.SetVertices(Vertices);
            mesh.SetUVs(0, UVs);
            mesh.SetIndices(Indices, meshTopology, 0);

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            return mesh;
        }
    }
}
