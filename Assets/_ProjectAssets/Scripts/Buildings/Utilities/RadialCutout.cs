using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public struct RadialCutout : IComparable<RadialCutout>
    {
        public Vector2 FirstVector;
        public Vector2 SecondVector;

        public RadialCutout(Vector2 firstVector, Vector2 secondVector)
        {
            FirstVector = firstVector;
            SecondVector = secondVector;
        }

        public int CompareTo(RadialCutout other)
        {
            float angle = Vector2.SignedAngle(FirstVector, Vector2.right);
            float otherAngle = Vector2.SignedAngle(other.FirstVector, Vector2.right);
            return angle.CompareTo(otherAngle);
        }

        public bool TryMerge(RadialCutout otherCutout, out RadialCutout mergedCutout)
        {
            if (FirstVector == otherCutout.SecondVector)
            {
                FirstVector = otherCutout.FirstVector;
                mergedCutout = new(otherCutout.FirstVector, SecondVector);
                return true;
            }

            else if (SecondVector == otherCutout.FirstVector)
            {
                SecondVector = otherCutout.SecondVector;
                mergedCutout = new(FirstVector, otherCutout.SecondVector);
                return true;
            }

            mergedCutout = default;
            return false;
        }
    }
}