using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public static class GraphUtilities
    {
        public static Dictionary<int, List<int>> CreateIndicesDictionary(List<(int, int)> indices)
        {
            Dictionary<int, List<int>> indicesDictionary = new();

            for (int i = 0; i < indices.Count; i++)
            {
                (int, int) pair = indices[i];
                AddEntry(pair.Item1, pair.Item2);
                AddEntry(pair.Item2, pair.Item1);
            }

            void AddEntry(int key, int value)
            {
                if (indicesDictionary.ContainsKey(key) == false)
                {
                    List<int> list = new();
                    list.Add(value);
                    indicesDictionary.Add(key, list);
                }
                else
                {
                    indicesDictionary[key].Add(value);
                }
            }

            return indicesDictionary;
        }
    }
}

