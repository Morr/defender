using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    [RequireComponent(typeof(MeshFilter))]
    public class DynamicMeshController : MonoBehaviour
    {
        private static Dictionary<string, DynamicMeshEntry> DynamicMeshes { get; set; } = new();

        private MeshFilter MeshFilter { get; set; }
        private string MeshId { get; set; } = null;

        public bool TrySetExistingMesh(string meshId)
        {
            if (meshId == MeshId)
            {
                return true;
            }

            if (DynamicMeshes.TryGetValue(meshId, out DynamicMeshEntry entry))
            {
                ReleaseMesh();
                MeshId = meshId;
                MeshFilter.mesh = entry.Mesh;
                entry.ReferencesCounter += 1;
                return true;
            }

            return false;
        }

        public void SetMesh(string meshId, Mesh mesh)
        {
            ReleaseMesh();
            MeshId = meshId;
            MeshFilter.mesh = mesh;
            DynamicMeshes.Add(meshId, new DynamicMeshEntry(mesh));
        }

        protected virtual void Awake()
        {
            MeshFilter = GetComponent<MeshFilter>();
        }

        private void OnDestroy()
        {
            ReleaseMesh();
        }

        private void ReleaseMesh()
        {
            if (MeshId != null)
            {
                DynamicMeshEntry entry = DynamicMeshes[MeshId];
                entry.ReferencesCounter -= 1;

                if (entry.ReferencesCounter == 0)
                {
                    Destroy(entry.Mesh);
                    DynamicMeshes.Remove(MeshId);
                }

                MeshId = null;
            }
        }

        private class DynamicMeshEntry
        {
            public Mesh Mesh;
            public int ReferencesCounter = 1;

            public DynamicMeshEntry(Mesh mesh)
            {
                Mesh = mesh;
            }
        }
    }
}
