using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public static class GeometryUtilities
    {
        public static Vector3 To3DSpace(this Vector2 vector)
        {
            return new Vector3(vector.x, 0.0f, vector.y);
        }

        public static Vector2 To2DSpace(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.z);
        }

        public static List<Vector2> To2DSpace(Vector3[] points)
        {
            List<Vector2> flatPolygon = new();

            for (int i = 0; i < points.Length; i++)
            {
                flatPolygon.Add(points[i].To2DSpace());
            }

            return flatPolygon;
        }

        public static void TranslatePoints(List<Vector2> polygon, Vector2 translation)
        {
            for (int i = 0; i < polygon.Count; i++)
            {
                polygon[i] += translation;
            }
        }

        public static void RotatePoints(List<Vector2> polygon, float eulerAngle)
        {
            float radiansAngle = eulerAngle * Mathf.Deg2Rad;
            float sin = Mathf.Sin(radiansAngle);
            float cos = Mathf.Cos(radiansAngle);

            for (int i = 0; i < polygon.Count; i++)
            {
                Vector2 point = polygon[i];
                float x = point.x * cos - point.y * sin;
                float y = point.x * sin + point.y * cos;
                polygon[i] = new Vector2(x, y);
            }
        }

        public static Vector2 RotatePoint(this Vector2 point, float eulerAngle)
        {
            float radiansAngle = eulerAngle * Mathf.Deg2Rad;
            float sin = Mathf.Sin(radiansAngle);
            float cos = Mathf.Cos(radiansAngle);
            float x = point.x * cos - point.y * sin;
            float y = point.x * sin + point.y * cos;
            return new Vector2(x, y);
        }

        public static float CalculateSignedArea(List<Vector2> polygon)
        {
            float signedArea = 0.0f;
            
            for (int i = 0; i < polygon.Count; i++)
            {
                int nextPointIndex = polygon.GetLoopedIndex(i + 1);
                Vector2 point = polygon[i];
                Vector2 nextPoint = polygon[nextPointIndex];
                signedArea += (point.x * nextPoint.y - nextPoint.x * point.y);
            }

            return signedArea * 0.5f;
        }

        public static Vector3 CalculateCentroid(IList<Vector3> collection, IList<int> indices = null)
        {
            Vector3 centroid = Vector3.zero;

            if (indices == null)
            {
                for (int i = 0; i < collection.Count; i++)
                {
                    centroid += collection[i];
                }

                centroid /= collection.Count;
            }
            else
            {
                for (int i = 0; i < indices.Count; i++)
                {
                    int index = indices[i];
                    centroid += collection[index];
                }

                centroid /= indices.Count;
            }

            return centroid;
        }

        //public static bool TryGetIntersectionPoint(Ray2D ray, Vector2 firstPoint, Vector2 secondPoint, out Vector2 crossingPoint)
        //{
        //    Vector2 b1 = ray.origin;
        //    Vector2 b2 = ray.origin + ray.direction;

        //    float tmp = (b2.x - b1.x) * (secondPoint.y - firstPoint.y) - (b2.y - b1.y) * (secondPoint.x - firstPoint.x);

        //    if (tmp == 0)
        //    {
        //        crossingPoint = default;
        //        return false;
        //    }

        //    float mu = ((firstPoint.x - b1.x) * (secondPoint.y - firstPoint.y) - (firstPoint.y - b1.y) * (secondPoint.x - firstPoint.x)) / tmp;
        //    crossingPoint = new Vector2(b1.x + (b2.x - b1.x) * mu, b1.y + (b2.y - b1.y) * mu);

        //    Vector2 v1 = crossingPoint - firstPoint;
        //    Vector2 v2 = crossingPoint - secondPoint;
        //    return Vector2.Dot(v1, v2) < 0.0f;
        //}
    }

}
