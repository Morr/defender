using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class BuildingRoot : MonoBehaviour
    {
        [field: SerializeField]
        public BuildingData BuildingData { get; set; }
        [field: SerializeField]
        public GameObject GeneratedContentParent { get; set; }

        public void UpdateVisualization()
        {
            DestroyImmediate(GeneratedContentParent);
            //GeneratedContentParent = BuildingGenerator.Instance.CreateBuilding(Buildingdata);
            GeneratedContentParent.transform.SetParent(transform);
        }
    }
}
