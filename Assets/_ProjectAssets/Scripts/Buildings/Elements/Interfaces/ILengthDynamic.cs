using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public interface ILengthDynamic
    {
        public void SetLength(float length);
    }
}
