using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public interface IRadialDynamic
    {
        void SetRadialSections(List<RadialCutout> cutouts);
    }
}
