using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public interface IHeightDynamic
    {
        public void SetHeight(float height);
    }
}
