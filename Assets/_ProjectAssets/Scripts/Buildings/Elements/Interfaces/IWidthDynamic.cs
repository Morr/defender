using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public interface IWidthDynamic
    {
        public void SetWidth(float width);
    }
}
