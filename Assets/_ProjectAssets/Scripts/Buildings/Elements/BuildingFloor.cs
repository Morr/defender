using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class BuildingFloor : MonoBehaviour
    {
        [field: SerializeField]
        private MeshFilter FloorMeshFilter { get; set; }
        [field: SerializeField]
        private MeshCollider MeshCollider { get; set; }

        [field: SerializeField, HideInInspector]
        private List<List<Vector2>> Polygons { get; set; }
        [field: SerializeField, HideInInspector]
        private float Thickness { get; set; }

        public void SetData(List<List<Vector2>> polygons, float thickness)
        {
            #if UNITY_EDITOR
            Polygons = polygons;
            Thickness = thickness;
            UnityEditor.EditorUtility.SetDirty(this);
            #endif

            CreateMesh();
        }

        private void CreateMesh()
        {
            Mesh[] floorSurfacesMeshes = new Mesh[Polygons.Count];
            Mesh[] floorSidesMeshes = new Mesh[Polygons.Count];

            for (int polygonIndex = 0; polygonIndex < Polygons.Count; polygonIndex++)
            {
                List<Vector2> polygon = Polygons[polygonIndex];
                MeshGenerationData[] generationData = MeshUtilities.CreatePrism(polygon.ToArray(), Thickness);
                floorSurfacesMeshes[polygonIndex] = generationData[0].CreateMesh();
                floorSidesMeshes[polygonIndex] = generationData[1].CreateMesh();
            }

            Mesh combinedSurfacesMesh = MeshUtilities.CombineMeshes(floorSurfacesMeshes, true);
            Mesh combinedSidesMesh = MeshUtilities.CombineMeshes(floorSidesMeshes, true);
            Mesh combinedFloorMesh = MeshUtilities.CombineMeshes(new Mesh[] { combinedSidesMesh, combinedSurfacesMesh }, false);
            ApplyMesh(combinedFloorMesh);
        }

        private void ApplyMesh(Mesh mesh)
        {
            FloorMeshFilter.sharedMesh = mesh;
            MeshCollider.sharedMesh = mesh;
        }
    }
}

