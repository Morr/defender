using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public class DeformableCorner : MonoBehaviour, IHeightDynamic, IRadialDynamic
    {
        [field: SerializeField]
        private GameObject Prefab { get; set; }
        [field: SerializeField]
        private float MeshHeight { get; set; } = 1.0f;
        [field: SerializeField]
        private float PlacementDepth { get; set; }
        [field: SerializeField]
        private Vector2[] ExcludedRadialRanges { get; set; }

        private Mesh OriginalMesh { get; set; }
        private float Height { get; set; }
        List<RadialCutout> RadialCutouts { get; set; }

        public void SetRadialSections(List<RadialCutout> radialCutouts)
        {
            RadialCutouts = radialCutouts;
            RebuildCorner();
        }

        public void SetHeight(float height)
        {
            Height = height;
            RebuildCorner();
        }

        protected virtual void Awake()
        {
            OriginalMesh = Prefab.GetComponent<MeshFilter>().sharedMesh;
        }

        private void RebuildCorner()
        {
            DestroySections();
            int verticalSectionsCount = Mathf.RoundToInt(Height / MeshHeight);

            if (verticalSectionsCount == 0)
            {
                return;
            }

            for (int i = 0; i < RadialCutouts.Count; i++)
            {
                RadialCutout cutout = RadialCutouts[i];
                RadialCutout nextCutout = RadialCutouts[RadialCutouts.GetLoopedIndex(i + 1)];
                CreateCornerSection(cutout.SecondVector, nextCutout.FirstVector, verticalSectionsCount);

                // Interior convex corner
                if (Vector2.SignedAngle(cutout.FirstVector, cutout.SecondVector) < 0.0f)
                {
                    CreateCornerSection(cutout.FirstVector, cutout.SecondVector, verticalSectionsCount);
                }
            }
        }

        private void DestroySections()
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i));
            }
        }

        private void CreateCornerSection(Vector2 firstSectionVector, Vector2 secondSectionVector, int verticalCount)
        {
            // Calculate angle
            float eulerAngle = Vector2.SignedAngle(firstSectionVector, secondSectionVector);
            eulerAngle = Mathf.Round(eulerAngle);

            if (eulerAngle <= 0.0f)
            {
                eulerAngle += 360.0f;
            }

            // Check if angle is excluded
            for (int i = 0; i < ExcludedRadialRanges.Length; i++)
            {
                Vector2 range = ExcludedRadialRanges[i];

                if (eulerAngle >= range.x && eulerAngle <= range.y)
                {
                    return;
                }
            }

            // Calculate offset
            float offsetAngle = eulerAngle > 180.0f ? (eulerAngle - 180.0f) : (180.0f - eulerAngle);
            offsetAngle *= 0.5f;
            float offset = (PlacementDepth * -1.0f) / Mathf.Cos(offsetAngle * Mathf.Deg2Rad);

            // Instantiate
            GameObject cornerSectionObject = Instantiate(Prefab, transform);

            // Set transforms
            float sectionObjectHeight = Height / verticalCount;
            cornerSectionObject.transform.forward = firstSectionVector.RotatePoint(eulerAngle * 0.5f).To3DSpace();
            cornerSectionObject.transform.localPosition = cornerSectionObject.transform.forward * offset;
            Vector3 scale = new Vector3(1.0f, sectionObjectHeight, 1.0f);
            cornerSectionObject.transform.localScale = scale;

            // Get deformed mesh
            string meshIdentifier = GetMeshIdentifier(eulerAngle);
            DynamicMeshController dynamicMeshController = cornerSectionObject.AddComponent<DynamicMeshController>();

            if (dynamicMeshController.TrySetExistingMesh(meshIdentifier) == false)
            {
                Mesh deformedMesh = CreateDeformedMesh(OriginalMesh, eulerAngle);
                dynamicMeshController.SetMesh(meshIdentifier, deformedMesh);
            }

            for (int i = 1; i < verticalCount; i++)
            {
                GameObject cornerSectionCopy = Instantiate(cornerSectionObject, transform);
                cornerSectionCopy.transform.localPosition += Vector3.up * sectionObjectHeight * i;
            }
        }

        private Mesh CreateDeformedMesh(Mesh originalMesh, float eulerAngle)
        {
            Mesh deformedMesh = Instantiate(originalMesh);
            Vector3[] vertices = originalMesh.vertices;

            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3 vertex = vertices[i];

                if (Mathf.Abs(vertex.x) > 0.01f)
                {
                    float y = vertex.y;
                    float rotationAngle = eulerAngle - 180.0f;
                    rotationAngle *= 0.5f;

                    if (vertex.x >= 0.0f)
                    {
                        rotationAngle *= -1.0f;
                    }

                    vertex = GeometryUtilities.RotatePoint(vertex.To2DSpace(), rotationAngle).To3DSpace();
                    vertex.y = y;
                }

                vertices[i] = vertex;
            }

            deformedMesh.vertices = vertices;
            deformedMesh.RecalculateBounds();
            deformedMesh.RecalculateNormals();
            deformedMesh.RecalculateTangents();
            return deformedMesh;
        }

        private string GetMeshIdentifier(float angle)
        {
            return Prefab.name + angle.ToString("0");
        }
    }
}
