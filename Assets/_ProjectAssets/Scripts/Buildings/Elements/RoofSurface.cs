using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class RoofSurface : MonoBehaviour, ILengthDynamic, IHeightDynamic
    {
        [field: SerializeField]
        private float LengthOverhang { get; set; }
        [field: SerializeField]
        private float HeightOverhang { get; set; }
        [field: SerializeField]
        private Transform MeshTransform { get; set; }

        public void SetLength(float length)
        {
            Vector3 newMeshScale = MeshTransform.localScale;
            newMeshScale.x = length + LengthOverhang * 2.0f;
            MeshTransform.localScale = newMeshScale;

            Vector3 newMeshPosition = MeshTransform.localPosition;
            newMeshPosition.x = length * 0.5f;
            MeshTransform.localPosition = newMeshPosition;
        }

        public void SetHeight(float height)
        {
            Vector3 newMeshScale = MeshTransform.localScale;
            newMeshScale.y = height + HeightOverhang;
            MeshTransform.localScale = newMeshScale;

            Vector3 newMeshPosition = MeshTransform.localPosition;
            newMeshPosition.y = (height + HeightOverhang) * -0.5f;
            MeshTransform.localPosition = newMeshPosition;
        }
    }
}
