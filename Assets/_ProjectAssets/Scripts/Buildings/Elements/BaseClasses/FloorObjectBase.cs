using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public abstract class FloorObjectBase : MonoBehaviour
    {
        public abstract List<Vector2> CalculateLocalCutout(float width, float length);
    }
}