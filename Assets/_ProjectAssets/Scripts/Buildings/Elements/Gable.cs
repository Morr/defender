using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class Gable : MonoBehaviour, IHeightDynamic, ILengthDynamic
    {
        public void SetLength(float length)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = length;
            transform.localScale = newScale;
        }

        public void SetHeight(float height)
        {
            Vector3 newScale = transform.localScale;
            newScale.y = height;
            transform.localScale = newScale;
        }
    }

}
