using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public class SimpleStairs : FloorObjectBase, IHeightDynamic, IWidthDynamic, ILengthDynamic
    {
        [field: SerializeField, Space]
        private GameObject StepPrefab { get; set; }
        [field: SerializeField]
        public float StepLength { get; private set; }
        [field: SerializeField]
        public float StepHeight { get; private set; }

        private GameObject[] Steps { get; set; }

        public override List<Vector2> CalculateLocalCutout(float width, float length)
        {
            float halfWidth = width * 0.5f;
            List<Vector2> localCutout = new();
            localCutout.Add(new Vector2(0.0f, halfWidth));
            localCutout.Add(new Vector2(length, halfWidth));
            localCutout.Add(new Vector2(length, -halfWidth));
            localCutout.Add(new Vector2(0.0f, -halfWidth));
            return localCutout;
        }

        public void SetWidth(float width)
        {
            transform.SetLocalScaleComponent(0, width);
        }

        public void SetLength(float length)
        {
            transform.SetLocalScaleComponent(2, length);
        }

        public void SetHeight(float height)
        {
            RebuildSteps(height);
        }

        private void RebuildSteps(float height)
        {
            int stepsCount = Mathf.RoundToInt(height / StepHeight);
            float generatedHeight = StepHeight * stepsCount;

            float generatedLength = StepLength * stepsCount;
            float scaledStepLength = (1.0f / generatedLength) * StepLength;

            Vector3 localStepPosition = Vector3.zero;
            Steps?.DestroyGameObjects();
            Steps = new GameObject[stepsCount];

            for (int i = 0; i < stepsCount; i++)
            {
                GameObject stepObject = Instantiate(StepPrefab, transform);
                stepObject.transform.localPosition = localStepPosition;
                stepObject.transform.SetLocalScaleComponent(2, scaledStepLength / StepLength);
                localStepPosition += new Vector3(0.0f, StepHeight, scaledStepLength);
                Steps[i] = stepObject;
            }

            transform.SetLocalScaleComponent(1, height / generatedHeight);
        }
    }
}
