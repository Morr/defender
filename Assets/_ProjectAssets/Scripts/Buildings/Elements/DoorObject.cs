using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    public class DoorObject : WallObjectBase, IHeightDynamic
    {
        [field: SerializeField]
        private Transform TopPartTransform { get; set; }

        public void SetHeight(float height)
        {
            Vector3 newTopPartScale = TopPartTransform.localScale;
            newTopPartScale.y = height - TopPartTransform.localPosition.y;
            TopPartTransform.localScale = newTopPartScale;
        }
    }
}

