using UnityEngine;
using UnityEditor;
using System;

namespace Defender.Buildings
{
    public class BuildingEditorWindow : EditorWindow
    {
        [MenuItem("Tools/Building Editor")]
        static void Init()
        {
            BuildingEditorWindow window = (BuildingEditorWindow)EditorWindow.GetWindow(typeof(BuildingEditorWindow));
            window.titleContent = new GUIContent("Building Editor");
            window.Show();
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        protected void OnGUI()
        {
            if (IsBuildingObjectSelected(out BuildingRoot buildingRoot) == true)
            {
                buildingRoot.BuildingData.BuildingIdentifier = EditorGUILayout.TextField(new GUIContent("Name"), buildingRoot.BuildingData.BuildingIdentifier);
            }
            else
            {
                if (GUILayout.Button("New Building", GUILayout.Height(50)))
                {
                    GameObject newBuildingObject = CreateNewBuildingObject();
                    Selection.objects = new GameObject[] { newBuildingObject };
                }
            }
        }

        private bool IsBuildingObjectSelected(out BuildingRoot buildingRoot)
        {
            GameObject selectedObject = Selection.activeObject as GameObject;

            if (selectedObject != null)
            {
                buildingRoot = selectedObject.GetComponentInParent<BuildingRoot>();

                if (buildingRoot != null)
                {
                    return true;
                }
            }

            buildingRoot = null;
            return false;
        }

        private GameObject CreateNewBuildingObject()
        {
            GameObject newBuildingObject = new GameObject("New Building");
            newBuildingObject.transform.position = Vector3.zero;
            newBuildingObject.transform.rotation = Quaternion.identity;
            newBuildingObject.AddComponent<BuildingRoot>();
            EditorUtility.SetDirty(newBuildingObject);
            return newBuildingObject;
        }
    }
}
