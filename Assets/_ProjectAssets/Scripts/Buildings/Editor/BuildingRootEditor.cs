using Defender.Buildings;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    [CustomEditor(typeof(BuildingRoot)), CanEditMultipleObjects]
    public class BuildingRootEditor : Editor
    {
        private float POINT_HANDLE_DIAMETER = 0.05f;

        private BuildingRoot BuildingRootComponent { get; set; }
        private List<Vector3> BuildingPoints { get; set; }

        private List<int> SelectedPointsIndices { get; set; } = new();
        private int HoveredPointIndex { get; set; } = new();

        private bool IsRepaintRequired { get; set; }

        protected virtual void OnEnable()
        {
            BuildingRootComponent = (BuildingRoot)target;
            BuildingPoints = BuildingRootComponent.BuildingData.Points;
            SelectedPointsIndices.Clear();
        }

        protected virtual void OnSceneGUI()
        {
            Event guiEvent = Event.current;
            DrawPointsHandles();
            DrawPositionHandle();

            if (guiEvent.type != EventType.Repaint && guiEvent.type != EventType.Layout)
            {
                ProcessMouseEvents(guiEvent);
            }

            if (guiEvent.type == EventType.Layout)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
            }

            if (IsRepaintRequired == true)
            {
                HandleUtility.Repaint();
                IsRepaintRequired = false;
            }
        }

        private void DrawPointsHandles()
        {
            for (int i = 0; i < BuildingPoints.Count; i++)
            {
                Vector3 currentPosition = BuildingPoints[i];
                DrawPointHandle(currentPosition, i);
            }
        }

        private void DrawPointHandle(Vector3 position, int index)
        {
            Color handleColor = index == (HoveredPointIndex) ? Color.yellow : Color.white;
            handleColor.a = SelectedPointsIndices.Contains(index) == true ? 0.65f : 0.25f;
            Handles.color = handleColor;
            Handles.SphereHandleCap(0, position, Quaternion.identity, POINT_HANDLE_DIAMETER, EventType.Repaint);
        }

        private void DrawPositionHandle()
        {
            if (SelectedPointsIndices.Count > 0)
            {
                Vector3 handlePosition = BuildingPoints[SelectedPointsIndices.GetLastElement()];
                Vector3 newHandlePosition = Handles.PositionHandle(handlePosition, BuildingRootComponent.transform.rotation);
                Vector3 positionDelta = newHandlePosition - handlePosition;

                if (positionDelta != Vector3.zero)
                {
                    Undo.RecordObject(BuildingRootComponent, "Modify building points positions");

                    for (int i = 0; i < SelectedPointsIndices.Count; i++)
                    {
                        int selectedPointIndex = SelectedPointsIndices[i];
                        BuildingPoints[selectedPointIndex] += positionDelta;
                    }
                }
            }
        }

        private void ProcessMouseEvents(Event guiEvent)
        {
            Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);
            UpdateHoveredPointIndex(mouseRay);

            if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0)
            {
                if (guiEvent.control == true)
                {
                    Plane plane = new Plane(Vector3.up, Vector3.zero);
                    plane.Raycast(mouseRay, out float distance);
                    AddPoint(mouseRay.GetPoint(distance));
                }
                else
                {
                    UpdateSelection(guiEvent);
                }
            }
        }

        private void UpdateHoveredPointIndex(Ray mouseRay)
        {
            HoveredPointIndex = -1;
            float minSqrDistance = Mathf.Infinity;

            for (int i = 0; i < BuildingRootComponent.BuildingData.Points.Count; i++)
            {
                Vector3 point = BuildingRootComponent.BuildingData.Points[i];

                if (CalculatePointRayDistance(point, mouseRay) <= POINT_HANDLE_DIAMETER * 0.5f)
                {
                    float distance = Vector3.SqrMagnitude(point - mouseRay.origin);

                    if (distance < minSqrDistance)
                    {
                        HoveredPointIndex = i;
                    }
                }
            }
        }

        private void UpdateSelection(Event guiEvent)
        {
            bool isAnyPointHovered = HoveredPointIndex >= 0;
            bool isHoveredPointSelected = SelectedPointsIndices.Contains(HoveredPointIndex);

            if (guiEvent.shift == true)
            {
                if (isAnyPointHovered == true)
                {
                    if (isHoveredPointSelected == true)
                    {
                        SelectedPointsIndices.Remove(HoveredPointIndex);
                        Debug.Log($"Removing selection {HoveredPointIndex}");
                    }
                    else
                    {
                        SelectedPointsIndices.Add(HoveredPointIndex);
                        Debug.Log($"Adding selection {HoveredPointIndex}");
                    }
                }
            }
            else
            {
                SelectedPointsIndices.Clear();

                if (isAnyPointHovered == true)
                {
                    SelectedPointsIndices.Add(HoveredPointIndex);
                }
            }

            IsRepaintRequired = true;
        }

        private float CalculatePointRayDistance(Vector3 point, Ray ray)
        {
            return Vector3.Cross(ray.direction, point - ray.origin).magnitude;
        }

        private void AddPoint(Vector3 position)
        {
            Undo.RecordObject(BuildingRootComponent, "Add building point");
            BuildingRootComponent.BuildingData.Points.Add(position);
        }
    }
}
