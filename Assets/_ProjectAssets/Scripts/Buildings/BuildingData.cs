using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    [System.Serializable]
    public class BuildingData
    {
        public string BuildingIdentifier;
        public List<Vector3> Points;
        public List<(int, int)> Connections;
    }

    [System.Serializable]
    public class Surface
    {
        public SurfaceType Type;
        public List<int> Indices;
        public List<SurfaceObject> Objects;
    }

    [System.Serializable]
    public class SurfaceObject
    {
        public string PrefabIdentifier;
        public Vector2 Size;
        public Vector2 Position;
        public Vector2 UpDirection;
    }

    public enum SurfaceType
    {
        Wall = 0,
        Floor = 1,
        Roof = 2
    }
}
