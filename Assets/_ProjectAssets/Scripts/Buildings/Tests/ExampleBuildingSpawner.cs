using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Defender.Buildings
{
    public class ExampleBuildingSpawner : MonoBehaviour
    {
        [field: SerializeField]
        private BuildingGenerator BuildingsGenerator { get; set; }
        [field: SerializeField, RenameProperty("First Floor")]
        private bool IsFirstFloorCreated { get; set; } = true;
        [field: SerializeField, RenameProperty("Second Floor")]
        private bool IsSecondFloorCreated { get; set; } = true;
        [field: SerializeField, RenameProperty("Labirynth")]
        private bool IsTestLabirynthCreated { get; set; } = false;

        protected virtual void Start()
        {
            CreateExampleBuilding();
        }

        private void CreateExampleBuilding()
        {
            BuildingDataDeprecated exampleBuildingData = CreateExampleBuildingData();
            GameObject exampleBuilding = BuildingsGenerator.CreateBuilding(exampleBuildingData);

            exampleBuilding.transform.position = Vector3.zero;
            exampleBuilding.transform.rotation = Quaternion.identity;
        }

        private BuildingDataDeprecated CreateExampleBuildingData()
        {
            BuildingDataDeprecated buildingData = new BuildingDataDeprecated();
            buildingData.Name = "House";

            buildingData.Floors = new ();

            if (IsTestLabirynthCreated == true)
            {
                buildingData.Floors.Add(CreateTestLabirynthFloorData());
            }

            if (IsFirstFloorCreated == true)
            {
                buildingData.Floors.Add(CreateFirstFloorData());
            }

            if (IsSecondFloorCreated == true)
            {
                buildingData.Floors.Add(CreateSecondFloorData());
            }

            return buildingData;
        }

        private FloorData CreateFirstFloorData()
        {
            List<Vector3> points = new(CreateRectanglePoints(5.0f, 3.0f));
            Vector3[] hallPoints = CreateRectanglePoints(2.0f, 1.0f);

            for (int i = 0; i < hallPoints.Length; i++)
            {
                hallPoints[i] += new Vector3(0.5f, 0.0f, -2.0f);
            }

            points.Insert(3, hallPoints[1]);
            points.Insert(4, hallPoints[2]);
            points.Insert(5, hallPoints[3]);
            points.Insert(6, hallPoints[0]);

            List<WallData> walls = CreateWalls(points);

            WallObjectData doorSection = new WallObjectData("ExteriorDoor", 1.0f, 0.9f);
            walls[4].AddSection(doorSection);

            FloorData floor = new FloorData()
            {
                Height = 2.5f,
                Thickness = 0.1f,
                Points = points,
                Walls = walls
            };

            FloorObjectData stairsData = new FloorObjectData("SimpleStairs", Vector3.zero, Vector2.left, 1.2f, 2.5f);
            floor.FloorObjects.Add(stairsData);

            return floor;
        }

        private FloorData CreateSecondFloorData()
        {
            List<Vector3> points = new (CreateRectanglePoints(5.5f, 3.5f));
            List<WallData> walls = CreateWalls(points);

            //points.Add(new Vector3(-4.0f, 0.0f, 1.75f));
            //points.Add(new Vector3(-4.0f, 0.0f, -1.75f));
            //walls.Add(new WallData(3, 5));
            //walls.Add(new WallData(5, 4));
            //walls.Add(new WallData(0, 4));

            FloorData floor = new FloorData()
            {
                Height = 1.8f,
                Thickness = 0.1f,
                Points = points,
                Walls = walls
            };

            return floor;
        }

        private FloorData CreateTestLabirynthFloorData()
        {
            List<Vector3> nodes = new();

            nodes.Add(new Vector3(1.0f, 0.0f, 1.0f));
            nodes.Add(new Vector3(1.0f, 0.0f, -1.0f));
            nodes.Add(new Vector3(-1.0f, 0.0f, -1.0f));
            nodes.Add(new Vector3(-1.0f, 0.0f, 1.0f));

            nodes.Add(new Vector3(0.1f, 0.0f, 1.0f));
            nodes.Add(new Vector3(0.1f, 0.0f, -0.1f));
            nodes.Add(new Vector3(-0.2f, 0.0f, -0.1f));
            nodes.Add(new Vector3(-0.2f, 0.0f, 0.7f));

            nodes.Add(new Vector3(-0.7f, 0.0f, 0.7f));
            nodes.Add(new Vector3(-0.7f, 0.0f, -0.7f));
            nodes.Add(new Vector3(0.7f, 0.0f, -0.7f));
            nodes.Add(new Vector3(0.7f, 0.0f, 1.0f));

            List<WallData> walls = CreateWalls(nodes);

            for (int i = 0; i < nodes.Count; i++)
            {
                nodes[i] *= 2.0f;
            }

            FloorData floor = new FloorData()
            {
                Height = 1.8f,
                Thickness = 0.1f,
                Points = nodes,
                Walls = walls
            };

            return floor;
        }

        private Vector3[] CreateRectanglePoints(float x, float z)
        {
            float halfWidth = z * 0.5f;
            float halfLength = x * 0.5f;

            Vector3[] points = new Vector3[]
            {
                new Vector3(-halfLength, 0.0f, halfWidth),
                new Vector3(halfLength, 0.0f, halfWidth),
                new Vector3(halfLength, 0.0f, -halfWidth),
                new Vector3(-halfLength, 0.0f, -halfWidth)
            };

            return points;
        }

        private List<WallData> CreateWalls(List<Vector3> nodes)
        {
            List<WallData> walls = new();

            for (int i = 0; i < nodes.Count; i++)
            {
                int endNodeIndex = nodes.GetLoopedIndex(i + 1);
                walls.Add(new WallData(i, endNodeIndex));
            }

            return walls;
        }
    }
}
