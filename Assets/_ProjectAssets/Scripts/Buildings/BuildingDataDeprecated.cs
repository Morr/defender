using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Defender.Buildings
{
    [System.Serializable]
    public class BuildingDataDeprecated
    {
        public string Name;
        public List<FloorData> Floors = new();
    }

    [System.Serializable]
    public class FloorData
    {
        public List<Vector3> Points;
        public List<WallData> Walls;
        public List<FloorObjectData> FloorObjects = new();
        public float Thickness;
        public float Height;
    }

    [System.Serializable]
    public class WallData
    {
        public int BeginPointIndex;
        public int EndPointIndex;
        public List<WallObjectData> WallObjects = new();

        public WallData(int beginNodeIndex, int endNodeIndex)
        {
            BeginPointIndex = beginNodeIndex;
            EndPointIndex = endNodeIndex;
            WallObjects = new();
        }

        public void AddSection(WallObjectData attachment)
        {
            WallObjects.Add(attachment);
        }
    }

    [System.Serializable]
    public class WallObjectData
    {
        public WallObjectData(string prefabIdentifier, float wallPosition, float length)
        {
            PrefabIdentifier = prefabIdentifier;
            WallPosition = wallPosition;
            Length = length;
        }

        public string PrefabIdentifier;
        public float WallPosition;
        public float Length;
    }

    [System.Serializable]
    public class FloorObjectData
    {
        public FloorObjectData(string prefabIdentifier, Vector3 position, Vector2 forward, float width, float length)
        {
            PrefabIdentifier = prefabIdentifier;
            Position = position;
            RightDirection = forward;
            Width = width;
            Length = length;
        }

        public string PrefabIdentifier;
        public Vector3 Position;
        public Vector2 RightDirection;
        public float Width;
        public float Length;
    }
}
